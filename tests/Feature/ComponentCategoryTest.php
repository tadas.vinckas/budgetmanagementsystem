<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Laravel\Passport\Passport;
use App\User;
use App\ComponentCategory;
use App\Services\TestService;

class ComponentCategoryTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;

    protected function setUp(): void
    {
        $this->createApplication();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testComponentCategoryIndex()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id
        ]);

        $this->get('/api/component-category')->assertStatus(200)->assertJsonFragment([
            "id" => $category->id
        ]);
    }

    public function testComponentCategoryCreate()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $data = [
            'name' => "test"
        ];

        $this->post('/api/component-category', $data)->assertStatus(201);

        $category = ComponentCategory::first();
        $this->assertNotNull($category);

        $this->post('/api/component-category', $data)->assertStatus(302); // test if cannot create category on existing name
    }


    public function testComponentCategoryUpdate()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id
        ]);

        $data = [
            'name' => "changedName"
        ];

        $this->put('/api/component-category/' . $category->id, $data)->assertStatus(200);

        $category = ComponentCategory::first();

        $this->assertEquals($category->name, $data["name"]);
    }

    public function testComponentCategoryDelete()
    {

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id
        ]);

        // test category delete
        $this->delete('/api/component-category/' . $category->id)->assertStatus(200);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id
        ]);

        (new TestService)->createTransaction($category->id, null, null, $user);

        // test category with existing transaction associated delete
        $this->delete('/api/component-category/' . $category->id)->assertStatus(422);
    }

    public function testSortCategories()
    {

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $categories = factory(ComponentCategory::class, 4)->create([
            "user_id" => $user->id
        ]);

        $categories[0]->save();
        $categories[1]->save();
        $categories[2]->save();
        $categories[3]->save();

        $data = ["sortedCategories" => [
            [
                'id' => $categories[1]->id
            ],
            [
                'id' => $categories[3]->id
            ],
            [
                'id' => $categories[2]->id
            ],
            [
                'id' => $categories[0]->id
            ],
        ]
        ];

        $this->post('/api/sort-categories', $data)->assertStatus(200);

        $category = ComponentCategory::find($data["sortedCategories"][0]["id"]);
        $this->assertEquals($category->position, "0");

        $category = ComponentCategory::find($data["sortedCategories"][1]["id"]);
        $this->assertEquals($category->position, "1");

        $category = ComponentCategory::find($data["sortedCategories"][2]["id"]);
        $this->assertEquals($category->position, "2");

        $category = ComponentCategory::find($data["sortedCategories"][3]["id"]);
        $this->assertEquals($category->position, "3");
    }
}
