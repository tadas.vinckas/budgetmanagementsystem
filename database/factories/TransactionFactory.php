<?php

use Faker\Generator as Faker;
use App\User;
use App\TransactionCategory;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Transaction::class, function (Faker $faker) {

    return [
        'beneficiary' => $faker->name,
        'date' => $faker->date,
        'processed' => 1
    ];
});
