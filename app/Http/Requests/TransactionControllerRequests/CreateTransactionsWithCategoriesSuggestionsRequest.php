<?php

namespace App\Http\Requests\TransactionControllerRequests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTransactionsWithCategoriesSuggestionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id.*' => 'required|integer',
            'beneficiary.*' => 'required|string|max:191',
            'date.*' => 'required|date',
            'transactionComponents.*.amount' => 'required|numeric',
            'transactionComponents.*.category_id' => 'required|numeric'
        ];
    }
}
