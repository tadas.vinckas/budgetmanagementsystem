<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use App\User;
use App\Transaction;
use App\ComponentCategory;
use App\TransactionComponent;
use App\CustomBeneficiary;
use App\BankBeneficiary;
use App\Services\TestService;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TransactionTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;
    protected function setUp(): void
    {
        $this->createApplication();

        // $this->withHeaders([
        //     'Accept' => 'application/json',
        //     'X-Requested-With' => 'XMLHttpRequest'
        // ]);

        parent::setUp();
    }

    protected function tearDown(): void
    {

        parent::tearDown();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testTransactionIndex()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        [$transaction,] = (new TestService)->createTransaction(null, null, null, $user);

        $this->get('/api/transaction?paginate=1')->assertStatus(200)
            ->assertJsonFragment([
                "id" => $transaction->id
            ]);
    }

    public function testTransactionUpdate()
    {
        $testService = new TestService;

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $categories = factory(ComponentCategory::class, 2)->create([
            "user_id" => $user->id
        ]);

        [$transaction, $customBeneficiary] = $testService->createTransaction(null, null, null, $user);

        $componentsData = [
            [
                "category_id" => $categories[0]->id,
                "amount" => 12,
                "id" => null
            ],
            [
                "category_id" => $categories[0]->id,
                "amount" => 13,
                "id" => null
            ]
        ];

        $data = [
            'id' => $transaction->id,
            'componentsToDelete' => [],
            'beneficiary_id' => $customBeneficiary->id,
            'beneficiary' => "maxima",
            'date' => "2020-08-03",
            'transactionComponents' => $componentsData,
            'componentsToDelete' => [$transaction->transactionComponents[0]["id"]]
        ];

        // test update
        $this->put('api/transaction/' . $transaction->id, $data)->assertStatus(200);

        $newTransaction = Transaction::find($transaction->id);
        $this->assertEquals($newTransaction->date, $data['date']);

        $newCustomBeneficiary = CustomBeneficiary::where('name',$data["beneficiary"])->first();
        $this->assertEquals($newCustomBeneficiary->id, $data["beneficiary_id"]);

        $newTransactionComponents = TransactionComponent::where("transaction_id", $transaction->id)->get();

        $this->assertEquals(count($newTransactionComponents),2);
        $this->assertEquals($newTransactionComponents[0]->amount, round($componentsData[0]["amount"],2));
        $this->assertEquals($newTransactionComponents[0]->category_id, $componentsData[1]["category_id"]);
        $this->assertEquals($newTransactionComponents[1]->amount, round($componentsData[1]["amount"],2));
        $this->assertEquals($newTransactionComponents[1]->category_id, $componentsData[0]["category_id"]);

    }

    public function testTransactionCreate()
    {
        $testService = new TestService;

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id
        ]);

        $componentsData = [
            [
                "category_id" => $category->id,
                "amount" => 12,
                "id" => null
            ]
        ];

        $data = [
            'id' => null,
            'componentsToDelete' => [],
            'beneficiary' => "maxima",
            'beneficiary_id' => null,
            'date' => "2020-08-03",
            'transactionComponents' => $componentsData,
            'processed' => 1
        ];

        // test create
        $this->post('/api/transaction', $data)->assertStatus(200);

        $newTransaction = Transaction::first();
        $this->assertEquals($newTransaction->date, $data['date']);

        $newCustomBeneficiary = CustomBeneficiary::where('name',$data["beneficiary"])->first();
        $this->assertNotNull($newCustomBeneficiary);

        $newTransactionComponents = TransactionComponent::where("transaction_id", $newTransaction->id)->get();

        $this->assertEquals(count($newTransactionComponents),1);
        $this->assertEquals($newTransactionComponents[0]->amount, round($componentsData[0]["amount"],2));
        $this->assertEquals($newTransactionComponents[0]->category_id, $componentsData[0]["category_id"]);
    }


    public function datesDataProvider()
    {
        return [
            'with_data' => [[
                'dateFrom' => "2020-02-14",
                'dateTo' => "2020-09-14",
                'allDataIncluded' => "true"
            ]],
            'without_data' => [[
                'dateFrom' => "null",
                'dateTo' => "null",
                'allDataIncluded' => "false"
            ]],
        ];
    }

    /**
     * @dataProvider datesDataProvider
     */
    public function testDistinctBeneficiaries($data)
    {
        $testService = new TestService;

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id
        ]);

        [, $customBeneficiary] = $testService->createTransaction($category->id, null, "2020-08-05", $user);
        [, $customBeneficiary2] = $testService->createTransaction($category->id, null, "2020-08-05", $user);

        $response = $this->get('/api/transaction/beneficiaries/' . $data["dateFrom"] . '/' . $data["dateTo"] . '/' . $category->id)->assertStatus(200);

        if ($data["allDataIncluded"]) {
            $response->assertJson([
                $customBeneficiary->name =>  $customBeneficiary->id,
            ]);
        } else {
            $response->assertJson([
                $customBeneficiary->name =>  $customBeneficiary->id,
                $customBeneficiary2->name => $customBeneficiary2->id
            ]);
        }
    }

    /**
     * @dataProvider datesDataProvider
     */
    public function testDistinctCategories($data)
    {
        $testService = new TestService;

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $categories = factory(ComponentCategory::class, 2)->create([
            "user_id" => $user->id
        ]);

        $testService->createTransaction($categories[0]->id, null, "2020-08-05", $user);
        $testService->createTransaction($categories[1]->id, null, "2020-12-05", $user);

        $response = $this->get('/api/transactions-categories/' . $data["dateFrom"] . '/' . $data["dateTo"])->assertStatus(200);

        if ($data["allDataIncluded"]) {
            $response->assertJson([
                $categories[0]->name => $categories[0]->id
            ]);
        } else {
            $response->assertJson([
                $categories[0]->name => $categories[0]->id,
                $categories[1]->name => $categories[1]->id
            ]);
        }
    }

    public function testTransactionDelete()
    {
        $testService = new TestService;

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        [$transaction,] = $testService->createTransaction(null, null, null, $user);

        // test transaction delete
        $this->delete(route('transaction.destroy', $transaction->id))->assertStatus(200);

        $transaction = Transaction::first();
        $this->assertNull($transaction);

        $transactionComponents = TransactionComponent::first();
        $this->assertNull($transactionComponents);
    }

    public function testTransactionSearch()
    {
        $testService = new TestService;

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $customBeneficiary = factory(CustomBeneficiary::class)->create([
            "user_id" => $user->id,
            "name" => "test"
        ]);

        $testService->createTransaction(null, $customBeneficiary->id, null, $user);
        [$transaction2,] = $testService->createTransaction(null, $customBeneficiary->id, null, $user);

        $this->get("/api/find-transaction?q=test")->assertStatus(200)->assertJsonFragment([
            "total" => 2
        ]);

        $customBeneficiary2 = factory(CustomBeneficiary::class)->create([
            "user_id" => $user->id,
            "name" => "differentname"
        ]);

        $transaction2->beneficiary = $customBeneficiary2->id;
        $transaction2->save();

        $this->get("/api/find-transaction?q=test")->assertStatus(200)->assertJsonFragment([
            "total" => 1
        ]);

        $this->get("/api/find-transaction")->assertStatus(200)->assertJsonFragment([
            "total" => 2
        ]);
    }
}
