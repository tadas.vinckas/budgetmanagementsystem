<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomBeneficiary extends Model
{
    protected $fillable = ["name","user_id","relatedToBank"];

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function bankBeneficiaries()
    {
       return $this->belongsToMany('App\BankBeneficiary','bank_benef_custom_benef');
    }

    public function scopeOwnUser($query)
    {
        return $query->where('user_id', auth('api')->user()->id);
    }

    public function scopeRelatedToBank($query, $related)
    {
        return $query->where('relatedToBank', $related);
    }

    public function user()
    {
       return $this->belongsTo(User::class);
    }
}
