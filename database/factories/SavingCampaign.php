<?php

use Faker\Generator as Faker;
use App\SavingCampaign;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\SavingCampaign::class, function (Faker $faker) {

 //   $user = User::where('name','test1234')->first();
    return [
        'name' => $faker->word,
        'date' => $faker->date,
        'income' => $faker->randomFloat(2,1,10000)
    ];
});
