<?php

namespace App\Services;

use App\ComponentCategory;
use App\Services\BeneficiaryService;
use App\Transaction;
use App\CustomBeneficiary;
use App\TransactionComponent;
use Carbon;

class TransactionService
{
    /** search transaction by text. Search based on beneficiary name */
    public function searchByText($text) { 
       if($text) { 
        return Transaction::ownUser()->processed(1)->whereHas('CustomBeneficiary', function ($query) use ($text) {
            $query->where('custom_beneficiaries.name', 'LIKE', "%$text%");
        })->with('CustomBeneficiary')->with('TransactionComponents.componentCategory')->paginate(10);
       }
       return;
    }

    public function parseBeneficiaryId($beneficiaryId) { 

        if ($beneficiaryId) {
            $beneficiary = CustomBeneficiary::ownUser()->findOrFail($beneficiaryId, ['id'])->toArray();
            return $beneficiary["id"];

        } else {
           return null;
        }
    }

    public function createTransaction($id, $date, $beneficiary, $beneficiaryId, $transactionComponents)
    {
        $beneficiaryService = new BeneficiaryService;
        $parsed_date = Carbon\Carbon::parse($date)->toDateString();

        $transaction = new Transaction();

        $beneficiaryId = $this->parseBeneficiaryId($beneficiaryId);

        // if transaction already created from statement
        if ($id) {
            $transaction = Transaction::findOrFail($id);

            $customBeneficiary = CustomBeneficiary::findOrFail($transaction->beneficiary);
            $customBeneficiary->relatedToBank = 1;
            $customBeneficiary->save();
        }

        $transaction->date = $parsed_date;
        $transaction->user_id = auth('api')->user()->id;
        $transaction->processed = 1;

        // get beneficiary depending if transaction was created in fast create mode
        $transaction->beneficiary = $beneficiaryId ? $beneficiaryId : $beneficiaryService->getTransactionBeneficiaryInStore($beneficiary);
        $transaction->save();

        // create components
        foreach ($transactionComponents as $transactionComp) {
            $this->createOrUpdateTransactionComponent(null, $transactionComp["category_id"], $transactionComp["amount"], $transaction->id);
        }
    }

    public function updateTransaction($id, $date, $beneficiary, $beneficiaryId, $transactionComponents, $componentsToDelete)
    {
        $beneficiaryService = new BeneficiaryService;

        $transaction = Transaction::findOrFail($id);

        $beneficiaryId = $this->parseBeneficiaryId($beneficiaryId);

        $transaction->beneficiary = $beneficiaryService->getTransactionBeneficiaryInUpdate($beneficiaryId, $beneficiary);
        $transaction->date = Carbon\Carbon::parse($date)->toDateString();

        $transaction->save();

        // update components
        foreach ($transactionComponents as $transactionComp) {
            $this->createOrUpdateTransactionComponent($transactionComp["id"], $transactionComp["category_id"], $transactionComp["amount"], $transaction->id);
        }

        $this->deleteComponents($componentsToDelete);
    }

    public function deleteComponents($components) { 
  
        if ($components) {
            foreach ($components as $id) {
                TransactionComponent::findOrFail($id)->delete();
            }
        }
    }

    public function createOrUpdateTransactionComponent($componentId, $categoryId, $amount, $transactionId) { 
        $transactionComponent = TransactionComponent::find($componentId);

        ComponentCategory::ownUser()->findOrFail($categoryId);

        if ($transactionComponent) {

            Transaction::ownUser()->findOrFail($transactionComponent->transaction_id);
            $transactionComponent->update([
                "category_id" => $categoryId,
                "amount" => round($amount, 2),
            ]);
        } else {
            $transactionComponent = new TransactionComponent([
                "category_id" => $categoryId,
                "amount" => round($amount, 2),
                "transaction_id" => $transactionId
            ]);
        }

        $transactionComponent->save();
    }

}
