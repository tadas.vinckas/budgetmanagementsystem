<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Transaction;
use App\Statement;
use App\Services\StatementService;
use App\TransactionComponent;
use Illuminate\Support\Collection;
use App\Http\Requests\StatementsControllerRequests\ImportStatementRequest;

class StatementsController extends Controller
{

     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->statementService = new StatementService;
    }

    public function importStatement(importStatementRequest $request)
    { 
        return $this->statementService->importStatement($request->file);
    }

    public function readImportedTransactions($id)
    {
        return $this->statementService->getTransactionsCategoriesSuggestions($id);
    }

    public function getStatements()
    {
        $statements = Statement::ownUser()->get();

        foreach ($statements as $statement) {
            $statement->processed_tr_count = Transaction::ownUser()->where("statement_id", $statement->id)->where('processed', 1)->count();
            $statement->not_processed_tr_count = Transaction::ownUser()->where("statement_id", $statement->id)->where('processed', 0)->count();
        }

        return $statements->paginate(5);
    }

    public function delete($id, $processed)
    {
        return $this->statementService->deleteStatement($id, $processed);
    }
}
