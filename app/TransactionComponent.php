<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use Auth;
use DB;
use App\Transaction;
use Illuminate\Database\Eloquent\Builder;

class TransactionComponent extends Model
{

    protected $fillable = ['amount', 'date', 'category_id', 'transaction_id'];

    public function user()
    {
        $this->belongsTo(Transaction::class);
    }

    public function componentCategory()
    {
        return $this->belongsTo(ComponentCategory::class, 'category_id');
    }

    public function transaction()
    {
        return $this->belongsTo(Transaction::class, 'transaction_id');
    }

    public function scopeOwnUser($query)
    {
        return $query->where('user_id',  auth('api')->user()->id);
    }
}
