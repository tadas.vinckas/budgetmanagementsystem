<?php

namespace App\Services;

use App\ComponentCategory;
use App\Services\StatsCalcService;
use Carbon;
use App\SavingCampaign;

class SavingCampaignService
{
    /** get first and last month day */
    public function prepareFirstAndLastMonthDates($month) { 
        $dates = array("first" => "", "last" => "");

        $dateFirst = strtotime('first day of this month', strtotime($month));
        $dateLast = strtotime('last day of this month', strtotime($month));

        $dates["first"] = date("Y-m-d", $dateFirst);
        $dates["last"] = date("Y-m-d", $dateLast);

        return $dates;
    }

    public function computeCampaignStats($camp) { 
        $statsDataService = new StatsDataService;
        $statsCalcService = new StatsCalcService;

        $totalStats = array("amountSpent" => 0, "amountWanted" => 0, "savingStatus" => "");
        $campCategories = array();

        $spendsDiffFromIncome = 0;
        $savingAchievements = 0;

        $dates = $this->prepareFirstAndLastMonthDates($camp->date);

        // compute amount spent, get saving status for each category and compute total stats
        foreach ($camp->componentCategories as $cat) {
            
            // amount spent of category
            $cat["amountSpent"] = round($statsDataService->categoryOrBeneficiaryTransactions($cat["id"], null, $dates["first"], $dates["last"], null)->sum("amount"), 2);

            // total stats
            $totalStats["amountSpent"] += $cat["amountSpent"];
            $totalStats["amountWanted"] += $cat->pivot->wanted_amount;
        
            $diffInPercentage =  $statsCalcService->computeDifferenceInPercentage($cat->pivot->wanted_amount, $cat["amountSpent"]);
            // get status
            $cat["savingStatus"] = $statsCalcService->getSavingsStatus($diffInPercentage, true);
        }

        $campCategories = collect($camp->componentCategories)->sortByDesc("savingStatus");

        // saving status for total stats
        $totalSavingCompliance = $statsCalcService->computeDifferenceInPercentage($totalStats["amountWanted"], $totalStats["amountSpent"]);
        $totalStats["savingStatus"] = $statsCalcService->getSavingsStatus($totalSavingCompliance, true);

        // compare income with amount spent 
        $spendsDiffFromIncome = $camp["income"] - $totalStats["amountSpent"];

        // compute how successful was collecting
        $savingAchievements = $totalStats["amountWanted"] - $totalStats["amountSpent"];

        return [$campCategories, $spendsDiffFromIncome, $savingAchievements, $totalStats];
    }

    public function getCampaignStats($camp)
    {
        $statsCalcService = new StatsCalcService;

        if ($camp) {

            [$campCategories, $spendsDiffFromIncome, $savingAchievements, $totalStats] = $this->computeCampaignStats($camp);
            return [
                "found" => true,
                "campaignStats" => $campCategories->values()->all(),
                "totalStats" => $totalStats,
                "spendsDiffFromIncome" => round($spendsDiffFromIncome, 2),
                "savingAchievements" => round($savingAchievements, 2),
                "spendsDiffFromIncomeStatus" => $statsCalcService->getSavingsStatus($statsCalcService->computeDifferenceInPercentage($camp["income"], $totalStats["amountSpent"]), true), // test
                "savingAchievementsStatus" => $statsCalcService->getSavingsStatus($statsCalcService->computeDifferenceInPercentage($totalStats["amountWanted"], $totalStats["amountSpent"]), true),
                "active" => $camp["active"],
                "date" => $camp["date"],
                "name" => $camp["name"]
            ];
           
        } else {
            return ["found" => false];
        }
  
    }

    public function getActiveStatus($date)
    {
        $dates = $this->prepareFirstAndLastMonthDates($date);

        $dateFrom = Carbon\Carbon::parse($dates["first"])->startOfDay()->toDateString();
        $dateTo = Carbon\Carbon::parse($dates["last"])->endOfDay()->toDateString();

        $now = Carbon\Carbon::now()->startOfDay()->toDateString();

        if ($now >= $dateFrom && $dateTo < $now) { //  ended
            return "0";
        } else if ($dateFrom <= $now && $dateTo >= $now) { // active
            return "1";
        } else {
            return "2"; // upcoming 
        }
    }

    /** Prepare categories for saving campaign including computed annual average, past 30 days expenses */
    public function getCategoriesData()
    {
        $statsDataService = new StatsDataService;
        $statsCalcService = new StatsCalcService;

        $dates = array("now" => Carbon\Carbon::now(), "beforeYear" =>  "", "beforeMonth" => "");

        $dates["now"] = Carbon\Carbon::parse(Carbon\Carbon::now())->addDay()->toDateString();
        $dates["beforeYear"] = Carbon\Carbon::parse(Carbon\Carbon::now())->subYear(1)->toDateString();
        $dates["beforeMonth"] = Carbon\Carbon::parse(Carbon\Carbon::now())->subMonth(1)->toDateString();

        // prepare categories stats for past month
        $categories = $statsCalcService->getCategoriesStatsData($dates["beforeMonth"], $dates["now"], true, false, false);

        $results = array();

        $minDate = $statsDataService->categoryOrBeneficiaryTransactions(null, null, $dates["beforeYear"], $dates["now"], null)->min("date");

        $daysDiff = $statsDataService->dateDifference(date($minDate), date($dates["now"]));

        // get annual monthly average for each category
        foreach ($categories["results"] as $cat) {

            $transactions = $statsDataService->categoryOrBeneficiaryTransactions($cat["id"], null, $dates["beforeYear"], $dates["now"], null)->get();
            $cat["yearAvg"] = $statsCalcService->computeExpensesAnnualMonthlyAverage($transactions->sum("amount"), $daysDiff);

            $results[] = $cat;
        }

        return $results;
    }

    public function storeSavingCampaign($name, $date, $income, $categories) { 

        $camp = new SavingCampaign; 
        $camp->name = $name;
        $camp->date = Carbon\Carbon::parse($date)->toDateString();
        $camp->user_id = auth('api')->user()->id;
        $camp->income = round($income, 2);
        $camp->active = $this->getActiveStatus($camp->date);

        $camp->save();

        $camp->wanted_amounts_sum = 0;

         // set components categories wanted amounts
        foreach ($categories as $category) {
            ComponentCategory::ownUser()->findOrFail($category["id"]);
            $camp->componentCategories()->attach($category["id"], ["wanted_amount" => round($category["wanted_amount"], 2)]);
            $camp->wanted_amounts_sum += $category["wanted_amount"];
        }

        $camp->save();

        return $camp->active;
    }

    public function updateSavingCampaign($id, $name, $date, $income, $categories) { 
   
        $campaign = SavingCampaign::ownUser()->find($id);

        $campaign->name = $name;
        $campaign->date = Carbon\Carbon::parse($date)->toDateString();
        $campaign->income = round($income, 2);
        $campaign->active = $this->getActiveStatus($campaign->dateFrom, $campaign->dateTo);

        $campaign->componentCategories()->sync([]);

        $campaign->save();

        $campaign->wanted_amounts_sum = 0;

        // set components categories wanted amounts
        foreach ($categories as $cat) {
            if(ComponentCategory::ownUser()->findOrFail($cat["id"])) { 
            $campaign->componentCategories()->attach($cat["id"], ["wanted_amount" => round($cat["wanted_amount"], 2)]);
            $campaign->wanted_amounts_sum += $cat["wanted_amount"];
            }
        }

        $campaign->save();

        return $campaign->active;
    }
}
