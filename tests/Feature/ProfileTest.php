<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Laravel\Passport\Passport;
use App\User;
use App\ComponentCategory;
use App\Services\TestService;

class ProfileTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;

    protected function setUp(): void
    {
        $this->createApplication();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testProfileGet() { 
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $this->get('api/profile')->assertStatus(200)->assertJson([
            "email" => $user->email,
            "name" => $user->name
        ]);
    }

    // public function testProfileUpdate() { 
    //     $user = factory(User::class)->create();
    //     Passport::actingAs($user);

    //     $data = [
    //         "email" => "new@email.com",
    //         "name" => "new name"
    //     ];
    //     $response = $this->patch('api/profile/' . $user->id, $data)->assertStatus(200);
    // }
}