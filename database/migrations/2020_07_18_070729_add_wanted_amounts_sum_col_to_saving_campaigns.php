<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWantedAmountsSumColToSavingCampaigns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('saving_campaigns', function (Blueprint $table) {
            $table->decimal("wanted_amounts_sum");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('saving_campaigns', function (Blueprint $table) {
            $table->dropColumn('wanted_amounts_sum');
        });
    }
}
