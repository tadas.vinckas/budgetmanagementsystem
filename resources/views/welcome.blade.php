<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Savex</title>
    <link rel="icon" href="/img/logo4.png" type="image/gif" sizes="16x16">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <link href="{{ asset('css/landing-page.css') }}" rel="stylesheet" type="text/css">
    <!-- Styles -->

</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
        <div class="container">
            <a class="navbar-brand" href="#">
            <img src="./img/logo4.png" width="35" height="35" alt=""> Savex
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">

                    @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('/dashboard') }}">Home
                        </a>
                    </li>
                    @else
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">Login</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('register') }}">Register</a>
                    </li>
                    @endauth
                </ul>
            </div>
        </div>
    </nav>

    <header class="jumbotron jumbotron-fluid">
        <div class="container-fluid text-center jumbotron-text">
            <h1>Saving money has never been easier</h1>
            <p>Free money collecting, expenses tracking website.</p>
            <p><a href="{{ route('register') }}" class="btn btn-lg" role="button">Get started</a></p>
        </div>
    </header>

    <section id="services" class="container">
   <!-- <h1 class="text-center">Our Services</h1>
    -->
   <div class="row services-row text-center">
   <div class="col-md-4 mb-4">
         <div class="card">
         <i class="far fa-credit-card fa-3x services-icon"></i>
            <div class="card-body">
               <h4 class="card-title">Transactions</h4>
               <p class="card-text">Easily create or import transactions from bank!</p>
            </div>
         </div>
      </div>
      
      <div class="col-md-4 mb-4">
         <div class="card">
        <i class="fas fa-chart-line fa-3x services-icon"></i>
            <div class="card-body">
               <h4 class="card-title">Analytics</h4>
               <p class="card-text">Track your money spending trends on many various charts!</p>
            </div>
         </div>
      </div>
      
      <div class="col-md-4 mb-4">
         <div class="card">
         <i class="fas fa-file-invoice-dollar fa-3x services-icon"></i>
            <div class="card-body">
               <h4 class="card-title">Reports</h4>
               <p class="card-text">Check out monthly reports about your expenses and collecting results!</p>
            </div>
         </div>
      </div>
   </div>

   <div class="row services-row text-center">
   <div class="col-md-4 mb-4">
         <div class="card">
         <i class="fas fa-wallet fa-3x services-icon"></i>
            <div class="card-body">
               <h4 class="card-title">Saving campaigns</h4>
               <p class="card-text">Set the amounts you would like to spend and monitor your progress!</p>
            </div>
         </div>
      </div>
      
      <div class="col-md-4 mb-4">
         <div class="card">
        <i class="fas fa-database fa-3x services-icon"></i>
            <div class="card-body">
               <h4 class="card-title">Easy access</h4>
               <p class="card-text">Store all your transtactions in one place!</p>
            </div>
         </div>
      </div>
      
      <div class="col-md-4 mb-4">
         <div class="card">
         <i class="fas fa-users fa-3x services-icon"></i>
            <div class="card-body">
               <h4 class="card-title">Customer support</h4>
               <p class="card-text">Consult our experts!</p>
            </div>
         </div>
      </div>
   </div>
</section>
</body>

</html>