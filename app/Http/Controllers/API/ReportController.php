<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\ReportService;

class ReportController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->reportService =  new ReportService;
    }
    
    public function report($date)
    {
        return $this->reportService->getReportData($date);
    }

    public function reportSummary($date) { 
    
       return $this->reportService->getReportSummaryData($date);
    }

}
