<?php


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('home');

// Route::get('invoice', function(){
//     return view('invoice');
// });

Route::get('{path}','HomeController@index')->where( 'path', '([\\w\-_]+)' );
