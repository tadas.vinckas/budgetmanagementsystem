<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class Transaction extends Model
{

    protected $fillable = ['beneficiary', 'date', 'user_id', 'processed', 'totalAmount', 'statement_id'];
    protected $primaryKey = 'id';
    protected $table = 'transactions';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactionComponentsSum()
    {
      return $this->select(\DB::raw('transactions, SUM(transaction_components.amount) as amountSum'))
        ->leftJoin('transaction_components', 'transaction_components.transaction_id', '=', 'transactions.id')
        ->groupBy('transactions.id');
    }

    public function customBeneficiary()
    {
        return $this->belongsTo(CustomBeneficiary::class, "beneficiary");
    }

    public function transactionComponents()
    {
        return $this->hasMany(TransactionComponent::class);
    }

    public function scopeOwnUser($query)
    {
        return $query->where('user_id',  auth('api')->user()->id);
    }

    public function scopeProcessed($query, $processed)
    {
        return $query->where('processed', $processed);
    }
}
