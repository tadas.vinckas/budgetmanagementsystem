<?php

namespace App\Services;

use App\SavingCampaign;
use App\Transaction;
use Carbon;
use App\Services\StatsDataService;
use App\Services\StatsCalcService;
use App\Services\SavingCampaignService;

class ReportService
{
    public function getReportData($date) { 

          $statsCalcService = new StatsCalcService;
          
          $dates = $this->getDates($date);
          
          $results = array(
              "totalExpenses" => "",
              "categoriesStats" => "",
              "dateIntervalFrom" => "",
              "dateIntervalTo" => "",
              "mostExpTransactions" => "",
              "mostExpBenef" => "",
              "savingCampaigns" => []
          ); 
  
          // get categories stats for selected month
          $categoriesStats = $statsCalcService->getCategoriesStatsData($dates["from"], $dates["to"], false, false, false);
          $categoriesStats = collect($categoriesStats["results"]);
          $categoriesStats = $categoriesStats->sortByDesc("amount")->values()->all();
  
          // get categories stats for previous from selected month
          $previousMonthCategoriesStats = $statsCalcService->getCategoriesStatsData($dates["prevMonthFrom"], $dates["prevMonthTo"], false, false, false);

          // get categories stats for year before from selected month
          $yearBeforeCategoriesStats = $statsCalcService->getCategoriesStatsData($dates["beforeYear"], $dates["prevMonthTo"], false, false, false);
          
          // get categories monthly average expenses stats from year before to selected month
          $yearBeforeCategoriesStatsAverages = $this->getExpensesAnnualMonthlyAverage($yearBeforeCategoriesStats,$dates["beforeYear"],$dates["prevMonthTo"]);
          
          // compare selected month with previous month
          $categoriesStats = $this->compareCategories($categoriesStats, $previousMonthCategoriesStats["results"], true);

          // compare selected month with year before
          $categoriesStats = $this->compareCategories($categoriesStats, $yearBeforeCategoriesStatsAverages["results"], false);
  
          $results["categoriesStats"] = $categoriesStats;
          $results["mostExpBenef"] = $this->mostExpBenef(5,false,$dates);
          $results["mostExpTransactions"] = $this->mostExpTransactions(5,false,$dates);
          $results["savingCampaigns"] = $this->formCampaignStats($dates["from"]);
  
          return $results;
    }

    /** get campaign stats for month */
    public function formCampaignStats($from) {
        $results = array(); 

        $month = date('m', strtotime($from));
        $year = date('Y', strtotime($from));
        
        // get ended or active campaign report
        $camp = SavingCampaign::ownUser()->where("active","<","2")->whereMonth("date","=",$month)->whereYear("date","=", $year)->first();
        if($camp) { 
        array_push($results, (new SavingCampaignService)->getCampaignStats($camp));
        }

        return $results;
    }

    /** get the most expensive beneficiaries */
    public function mostExpBenef($limit, $takeFirst, $dates)
    {
        $statsDataService = new StatsDataService;

        $benefStats = array("beneficiary" => "", "sum" => "");
        $benefCollection = collect();

        $beneficiaries = $statsDataService->getDistinctBeneficiaries($dates["from"], $dates["to"], null, null);

        foreach ($beneficiaries as $key => $ben) { // set beneficiaries sum
            $benefStats["beneficiary"] = $key;
            $benefStats["sum"] = $statsDataService->categoryOrBeneficiaryTransactions(null, $ben, $dates["from"], $dates["to"], null)->sum("amount");
            $benefCollection->push($benefStats);
        }

        $benefCollection = $benefCollection->sortByDesc('sum');

        if ($takeFirst) {
            return $benefCollection->first();
        } else {
            return $benefCollection->values()->take($limit);
        }
    }

    /** most expensive transactions */
    public function mostExpTransactions($limit, $takeFirst, $dates)
    {
        $transactions = Transaction::ownUser()->processed(1)->whereBetween('date', [$dates["from"], $dates["to"]])->select(\DB::raw('transactions.*, SUM(transaction_components.amount) as amountSum'))
            ->leftJoin('transaction_components', 'transaction_components.transaction_id', '=', 'transactions.id')
            ->groupBy('transactions.id')->with('customBeneficiary')
            ->orderBy('amountSum', 'DESC');

        if ($takeFirst) {
            return $transactions->first();
        } else {
            return $transactions->take($limit)->get();
        }
    }

    /** compute total sum of categories amounts */
    public function categoriesTotalSum($comparedCategories)
    {
        $sum = 0;

        foreach ($comparedCategories as $cat) {
            $sum += $cat["amount"];
        }

        return $sum;
    }

    /** get various dates for computing stats */
    public function getDates($date)
    {
        $dates = array("from" => "", "to" => "", "prevMonthFrom" => "", "prevMonthTo" => "", "beforeYear" => "", "now" => "");

        $date = substr($date, 0, 7);
        $fr = \DateTime::createFromFormat('Y-m', $date);

        $dates["from"] = $fr->format('Y-m-01');
        $dates["to"] = $fr->format('Y-m-31');
        
        $dates["now"] = Carbon\Carbon::parse(Carbon\Carbon::now())->addDay()->toDateString();

        $dates["beforeYear"] = date("Y-m-d", strtotime("-12 months", strtotime($dates["from"])));

        $dates["prevMonthFrom"] = date("Y-m-d", strtotime("-1 months", strtotime($dates["from"])));
        $dates["prevMonthTo"] = date("Y-m-31", strtotime("-1 months", strtotime($dates["to"])));

        return $dates;
    }
 
    /** compute given month expenses */
    public function monthlyExpenses($dates) 
    {
        $results = array("label" => "", "amount" => "", "status" => "4");

        $results["amount"] = round((new StatsDataService)->categoryOrBeneficiaryTransactions(null, null, $dates["from"], $dates["to"],null)->sum("amount"), 2);
    
        $results["label"] = "total current month expenses";

        return $results;
    }

    /** annual monthly expenses */
    public function annualMonthlyAvgExpenses($dates) { 

        $statsDataService = new StatsDataService;
        $statsCalcService = new StatsCalcService;

        $results = array("label" => "", "amount" => "", "status" => "4");

        // past year transactions
        $transactions = $statsDataService->categoryOrBeneficiaryTransactions(null, null, $dates["beforeYear"], $dates["now"],null)->get();

        // days difference between past year last transaction creation date to selected month
        $daysDiff = $statsDataService->dateDifference(date($transactions->min('date')), date($dates["now"]));

        // compute average
        $results["amount"] = round($statsCalcService->computeExpensesAnnualMonthlyAverage($transactions->sum("amount"), $daysDiff), 2);

        $results["label"] = "annual average expenses per month";

       return $results;
    }

    /** get information about saving achievements */
    public function savingsResults($dates, $monthlyAmount) { 

        $statsCalcService = new StatsCalcService;

        $results = array("label" => "", "amount" => null, "status" => "3");

        // get income
        $income = SavingCampaign::ownUser()->where("date", $dates["from"])->pluck("income")->first();

        // if saving campaign exists
        if($income) { 
        // compute balance
        $results["amount"] = $income - $monthlyAmount;

        // if positive number add +
        if($results["amount"] > 0) {
            $results["amount"] = "+" . round($results["amount"], 2);
        }

        else { 
            $results["amount"] = round($results["amount"], 2);
        }

        // compute difference of income and monthly amount in percentage
        $diffInPerc = $statsCalcService->computeDifferenceInPercentage($income, $monthlyAmount);

        // get saving status 
        $results["status"] = $statsCalcService->getSavingsStatus($diffInPerc, true);

        $results["label"] = "planned income compared to current month total expenses";
        }

        // if saving campaign doesn't exist
        else { 
            $results["amount"] = "Start collecting!";
            $results["label"] = "Create saving campaign by clicking more info";
        }

        return $results;
    }

    public function getReportSummaryData($date)
    {
        $dates = $this->getDates($date);
        $monthlyAmount = $this->monthlyExpenses($dates);
        $annualAvg = $this->annualMonthlyAvgExpenses($dates, $monthlyAmount["amount"]);
        $savingAchievements = $this->savingsResults($dates, $monthlyAmount["amount"]);
    
        return [
            "currMonth" => $monthlyAmount, "annualAvg" => $annualAvg, "savingAchievements" => $savingAchievements
        ];
    }
    
    public function computeExpensesAnnualMonthlyAverage($sum,$daysDiff) { 
        if($daysDiff == 0) { 
            return $sum;
        }
        else if($sum > 0)  {
            return round($sum / ($daysDiff / 30), 2);
        }
        else { 
            return 0;
        }
    }

    /** difference between two dates in days*/
    public function dateDifference($date_1, $date_2, $differenceFormat = '%a')
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);
    }

    /** compare current month expenses with past month 
     * or if compareWithPrevMonth is false - with annual monthly average of that category */
    public function compareCategories($current, $past, $compareWithPrevMonth)
    {

        $statsCalcService = new StatsCalcService;

        foreach ($current as &$curCat) {

            foreach ($past as $pastCat) {
                if ($curCat["name"] == $pastCat["name"]) {

                    [$diffInPerc, $diff, $diffInPercFormatted] = $statsCalcService->getNumbersDifference($pastCat["amount"], $curCat["amount"]);

                    if ($compareWithPrevMonth) {
                        $curCat["diffInPercPastMonth"] = $diffInPercFormatted;
                        $curCat["diffPastMonth"] = $diff;
                        $curCat["savingsStatusPastMonth"] = $statsCalcService->getSavingsStatus($diffInPerc, false);
                    } else {
                        $curCat["diffInPercYearAvg"] = $diffInPercFormatted;
                        $curCat["diffYearAvg"] = $diff;
                        $curCat["yearAvg"] = round($pastCat["amount"], 2);
                        $curCat["savingsStatusYearAvg"] = $statsCalcService->getSavingsStatus($diffInPerc, false);
                    }
                }
            }
        }

        return $current;
    }

    /** Sort and filter category beneficiaries */
    public function sortAndFilterCategoryBeneficiaries($results, $limit)
    {

        $beneficiaries = new Collection();

        foreach ($results as $result) {
            if ($result["amount"] > 0) {
                $beneficiaries->push($result);
            }
        }

        $beneficiaries = $beneficiaries->sortByDesc("amount");

        return $beneficiaries->values()->take($limit);
    }

    public function getExpensesAnnualMonthlyAverage($stats, $dateFrom, $dateTo)
    {

        $statsDataService = new StatsDataService;
        $statsCalcService = new StatsCalcService;

        $dateFrom = $statsDataService->categoryOrBeneficiaryTransactions(null, null, $dateFrom, $dateTo,null)->min("date");

        $daysDiff = $statsDataService->dateDifference(date($dateFrom),date($dateTo));

        foreach ($stats["results"] as &$categStats) {
            // compute annual monthly average
            $categStats["amount"] = $statsCalcService->computeExpensesAnnualMonthlyAverage($categStats["amount"],$daysDiff);
        }
        return $stats;
    }
}
