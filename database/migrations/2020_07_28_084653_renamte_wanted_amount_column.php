<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenamteWantedAmountColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('component_category_saving_campaign', function (Blueprint $table) {
            $table->renameColumn('wantedAmount', 'wanted_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('component_category_saving_campaign', function (Blueprint $table) {
            $table->renameColumn('wanted_amount', 'wantedAmount');
        });
    }
}
