<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Services\TestService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Laravel\Passport\Passport;
use App\CustomBeneficiary;
use App\ComponentCategory;

class StatsTest extends TestCase
{
   use WithoutMiddleware, RefreshDatabase;

   protected function setUp(): void
   {
      $this->createApplication();

      parent::setUp();
   }

   protected function tearDown(): void
   {
      parent::tearDown();
   }

   public function testMainChartsStats()
   {

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $categories = factory(ComponentCategory::class, 2)->create([
         "user_id" => $user->id,
      ]);

      $categories[0]->name = "nameone";
      $categories[0]->save();
      $categories[1]->name = "nametwo";
      $categories[1]->save();

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id,
         "name" => "nameone"
      ]);

      $customBeneficiary2 = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id,
         "name" => "nametwo"
      ]);

      [$transaction,] = (new TestService)->createTransaction($categories[1]->id, $customBeneficiary->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($categories[1]->id, $customBeneficiary->id, "2020-08-01", $user);
      [$transaction3, $customBeneficiary2] = (new TestService)->createTransaction($categories[1]->id, $customBeneficiary2->id, "2020-07-01", $user);
      [$transaction4,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary->id, "2020-08-01", $user);

      $response = $this->get('api/main-charts-stats/null/null')->assertStatus(200);

      $responseContent = json_decode($response->getContent());

      // test total stats
      $this->assertEquals($responseContent->totalStats->results[0]->date, "2020-07");
      $this->assertEquals($responseContent->totalStats->results[1]->date, "2020-08");

      $this->assertEquals($responseContent->totalStats->results[0]->sum, round($transaction->totalAmount + $transaction3->totalAmount, 2));
      $this->assertEquals($responseContent->totalStats->results[1]->sum, round($transaction2->totalAmount + $transaction4->totalAmount, 2));

      // test category stats
      $this->assertEquals($responseContent->categoryStats->results[0]->date, "2020-08");
      $this->assertEquals($responseContent->categoryStats->results[0]->sum, $transaction4->totalAmount);

      // test beneficiary stats
      $this->assertEquals($responseContent->beneficiaryStats->results[0]->date, "2020-07");
      $this->assertEquals($responseContent->beneficiaryStats->results[0]->sum, $transaction->totalAmount);
      $this->assertEquals($responseContent->beneficiaryStats->results[1]->date, "2020-08");
      $this->assertEquals($responseContent->beneficiaryStats->results[1]->sum, round($transaction2->totalAmount + $transaction4->totalAmount, 2));

      // test category beneficiary stats
      $this->assertEquals($responseContent->categoryBeneficiaryStats->results[0]->date, "2020-08");
      $this->assertEquals($responseContent->categoryBeneficiaryStats->results[0]->sum, $transaction4->totalAmount);

      // test categories stats
      $this->assertEquals($responseContent->categoriesStats->results[0]->name, $categories[0]->name);

      $this->assertEquals($responseContent->categoriesStats->results[0]->amount, $transaction4->totalAmount);

      $this->assertEquals($responseContent->categoriesStats->results[1]->name, $categories[1]->name);
      $amount = $transaction->totalAmount + $transaction2->totalAmount + $transaction3->totalAmount;
      $this->assertEquals($responseContent->categoriesStats->results[1]->amount, $amount);

      // test category beneficiaries stats
      if ($responseContent->categoryBeneficiariesStats->results[0]->amount != 0) {
         $this->assertEquals($responseContent->categoryBeneficiariesStats->results[0]->percentage, 100);
      } else {
         $this->assertEquals($responseContent->categoryBeneficiariesStats->results[0]->percentage, 0);
      }

      $this->assertEquals($responseContent->categoryBeneficiariesStats->results[0]->amount, $transaction4->totalAmount);
      $this->assertEquals($responseContent->categoryBeneficiariesStats->results[0]->beneficiary_name, $customBeneficiary->name);

      // test beneficiaries
      $this->assertEquals(isset($responseContent->beneficiaries->nameone), 1);
      $this->assertEquals(isset($responseContent->beneficiaries->nametwo), 2);

      // test category beneficiaries
      $this->assertEquals(isset($responseContent->categoryBeneficiaries->nameone), 1);

      // test categories
      $this->assertEquals(isset($responseContent->transactionsCategories->nameone), 1);
      $this->assertEquals(isset($responseContent->transactionsCategories->nametwo), 2);
   }
}