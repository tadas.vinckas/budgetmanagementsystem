<?php

namespace App;
use Auth;

use Illuminate\Database\Eloquent\Model;

class ComponentCategory extends Model
{
    protected $fillable = ['name', 'date', 'position', 'user_id'];
    public function transactionComponent() { 
        return $this->hasMany(TransactionComponent::class);
    }

    public function user() { 
        $this->belongsTo(User::class);
    }

    public function savingCampaign() { 
       return $this->belongsToMany(SavingCampaign::class)->withPivot('wanted_amount');
    }

    public function scopeOwnUser($query) { 
        return $query->where('user_id', auth('api')->user()->id);
    }
}
