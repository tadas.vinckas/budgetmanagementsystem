**Website link:**
 http://savex.herokuapp.com/

**Seeded profile data:**
email savex1@gmail.com
password savex1

**Website lets users:**
- Create mannualy transaction which consists of many components. For instance IKEA: 5€ household items, 20€ gifts
- Import transactions statements from bank in .csv format and easily process them
- See 6 various charts about expenses and 3 charts about collecting achievements in selected date interval
- See selected month report including many stats (it can be printed in pdf format)
- Create saving campaign which helps on planning monthly budget for every category and later following saving achievements in progress charts
- See other various stats

**Plans for the future:**
- Create admin page
- Refactor code with accesors and mutators
- Complete full tests
- Integrate mail with cron, observers,jobs, listeners and events
- Integrate categories special icons, colors
- Remake representation of complex stats to make user understand numbers easier
- Integrate ability to change language
- Refactor scopes to global scopes
- Use computed in Vue.js

**Things used / learned by doing this project:**
- Vue Router with Laravel
- AdminLTE 3
- How to Use API in Laravel
- Api Auth with Laravel Passport
- JWT with Laravel Passport and JavaScript Request
- Vue Custom Events
- Vue form with Laravel
- Relational Database with Laravel, many to many relationships, pivot tables
- Axios and Ajax Request
- PDF print, file import
- Charts with ChartJS
- Laravel middleware
- Laravel custom requests, services classes
- Deployment on Heroku
