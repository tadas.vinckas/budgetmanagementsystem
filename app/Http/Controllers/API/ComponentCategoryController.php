<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\ComponentCategory;
use App\TransactionComponent;
use App\Http\Requests\ComponentCategoryControllerRequests\ComponentCategoryStoreRequest;
use App\Http\Requests\ComponentCategoryControllerRequests\ComponentCategoryUpdateRequest;
use App\Http\Requests\ComponentCategoryControllerRequests\SortCategoriesRequest;
use Response;

class ComponentCategoryController extends Controller
{
       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request["paginate"] == 0) { // not paginate
            return ComponentCategory::ownUser()->orderBy("position")->get();
        }

        return ComponentCategory::ownUser()->latest()->paginate(5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    { }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComponentCategoryStoreRequest $request)
    {
        $category = ComponentCategory::create([
            "name" => $request["name"],
            "user_id" => auth('api')->user()->id
        ]);

        return $category;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ComponentCategoryUpdateRequest $request, $id)
    {
        $category = ComponentCategory::ownUser()->find($id);

        $category->name = $request->name;
        $category->save();

        return $category;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = ComponentCategory::ownUser()->findOrFail($id);

        if (TransactionComponent::where('category_id', $id)->first()) {
            return Response::json([
                'error' => "Transactions components with this category already exists"
            ], 422);
        } else {
            $category->delete();
            return ['message' => 'Deleted successfuly'];
        }
    }

    public function sortCategories(SortCategoriesRequest $request)
    {
        // set positions
        foreach ($request["sortedCategories"] as $key => $cat) {
            ComponentCategory::ownUser()->find($cat["id"])->update([
                'position' => $key
            ]);
        }

        return ['message' => 'Sorted successfully'];
    }
}
