<?php

namespace App\Http\Controllers\API;

use App\CustomBeneficiary;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Transaction;
use App\TransactionComponent;
use App\Services\StatsDataService;
use App\Services\TransactionService;
use App\Http\Requests\TransactionControllerRequests\TransactionStoreRequest;
use App\Http\Requests\TransactionControllerRequests\TransactionUpdateRequest;
use App\Http\Requests\TransactionControllerRequests\CreateTransactionsWithCategoriesSuggestionsRequest;

class TransactionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * @mixin \Eloquent
     * @mixin \Illuminate\Database\Eloquent\Builder
     */
    public function index()
    {
        return Transaction::ownUser()->processed(1)->orderBy('date', 'desc')->with('TransactionComponents.componentCategory')->with('CustomBeneficiary')->get()->paginate(8);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionStoreRequest $request)
    {
        $transactionService = new TransactionService;

        $transactionService->createTransaction($request["id"], $request["date"], $request["beneficiary"], $request["beneficiary_id"], $request["transactionComponents"]);

        return response()->json(['message' => 'Added successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(TransactionUpdateRequest $request, $id)
    {
        $transactionService = new TransactionService;
        
        $transactionService->updateTransaction($id, $request["date"], $request["beneficiary"], $request["beneficiary_id"], $request["transactionComponents"], $request["componentsToDelete"]);

        return ['message' => "Success"];
    }

    /** search for transactions by beneficiary */
    public function search()
    {

        $transactionService = new TransactionService;

        $searchResults = $transactionService->searchByText(\Request::get('q'));

        if ($searchResults) {
            return $searchResults;
        }

        return Transaction::ownUser()->processed(1)->with('TransactionComponents.componentCategory')->with('CustomBeneficiary')->get()->paginate(10);
    }

    /** create transactions with categories suggestions. Fast transaction create in wizard */
    public function createTransactionsWithCategoriesSuggestions(CreateTransactionsWithCategoriesSuggestionsRequest $request)
    {

        $transactionService = new TransactionService;

        foreach ($request->all() as $transaction) {

            $transactionService->createTransaction($transaction["id"], $transaction["date"], null, $transaction["beneficiary"], $transaction["transactionComponents"]);
        }

        return ['message' => "Success"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::ownUser()->findOrFail($id);

        TransactionComponent::where('transaction_id', $transaction->id)->delete();
        $transaction->delete();

        return ['message' => 'Transaction Deleted'];
    }

    /** get beneficiaries which have at least one transaction in date interval */
    public function distinctBeneficiaries($dateFrom, $dateTo, $categoryId)
    {
        $statsDataService = new StatsDataService;

        [$dateFrom, $dateTo] = $statsDataService->formatNullDates($dateFrom, $dateTo);

        return $statsDataService->getDistinctBeneficiaries(
            $dateFrom,
            $dateTo,
            $categoryId
        );
    }

    /** get categories which have at least one transaction in date interval */
    public function distinctCategories($dateFrom, $dateTo)
    {
        $statsDataService = new StatsDataService;

        [$dateFrom, $dateTo] = $statsDataService->formatNullDates($dateFrom, $dateTo);

        return $statsDataService->getDistinctCategories($dateFrom, $dateTo);
    }
}
