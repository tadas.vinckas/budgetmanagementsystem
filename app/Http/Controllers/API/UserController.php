<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CustomBeneficiary;
use App\Transaction;
use App\ComponentCategory;
use App\TransactionComponent;
use App\User;
use App\Statement;
use App\SavingCampaign;

use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('isAdmin');
        return User::latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('isAdmin');

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users',
             'password' => 'required|string|min:6'
        ]);

        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'type' => $request['type'],
            'bio' => $request['bio'],
          //  'photo' => $request['photo'],
            'password' => Hash::make($request['password']),
        ]);
    }


    public function updateProfile(Request $request)
    {
        $this->authorize('isAdmin');

        $user = auth('api')->user();


        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|required|min:6'
        ]);


        $currentPhoto = $user->photo;


        // if($request->photo != $currentPhoto){
        //     $name = time().'.' . explode('/', explode(':', substr($request->photo, 0, strpos($request->photo, ';')))[1])[1];

        //     \Image::make($request->photo)->save(public_path('img/profile/').$name);
        //     $request->merge(['photo' => $name]);

        //     $userPhoto = public_path('img/profile/').$currentPhoto;
        //     if(file_exists($userPhoto)){
        //         @unlink($userPhoto);
        //     }

        // }


        if(!empty($request->password)){
            $request->merge(['password' => Hash::make($request['password'])]);
        }


        $user->update($request->all());
        return ['message' => "Success"];
    }


    public function profile()
    {
        $this->authorize('isAdmin');

        return auth('api')->user();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('isAdmin');

        $user = User::findOrFail($id);

        $this->validate($request,[
            'name' => 'required|string|max:191',
            'email' => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password' => 'sometimes|min:6'
        ]);

        $user->update($request->all());
        return ['message' => 'Updated the user info'];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $this->authorize('isAdmin');

        $user = User::findOrFail($id);
        // delete the user

        $transactions = Transaction::ownUser()->where("user_id", $id)->get();
  
        $transactionsIds = $transactions->pluck("id");


        TransactionComponent::whereIn("transaction_id", $transactionsIds)->delete();

        foreach ($transactions as $transaction) {
            $transaction->delete();
        }

        $campaigns = SavingCampaign::ownUser()->where("user_id", $id)->get();

        foreach ($campaigns as $campaign) {
            $campaign->componentCategories()->detach();
            $campaign->delete();
        }

        ComponentCategory::ownUser()->where("user_id", $id)->delete();
        
        $customBeneficiaries = CustomBeneficiary::ownUser()->where("user_id", $id)->get();

        foreach($customBeneficiaries as $ben) { 
            $ben->bankBeneficiaries()->detach();
            $ben->delete();
        }

        Statement::ownUser()->where("user_id", $id)->delete();
        
        $user->delete();

        return ['message' => 'User Deleted'];
    }

    // public function search(){

    //     if ($search = \Request::get('q')) {
    //         $users = User::where(function($query) use ($search){
    //             $query->where('name','LIKE',"%$search%")
    //                     ->orWhere('email','LIKE',"%$search%");
    //         })->paginate(20);
    //     }else{
    //         $users = User::latest()->paginate(5);
    //     }

    //     return $users;

    // }
}
