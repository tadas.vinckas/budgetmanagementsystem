<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Services\StatsDataService;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatsDataTest extends TestCase
{
    public function testDateDifference()
    {
        $date1 = "2020-08-01";
        $date2 = "2020-09-15";
        $result = (new StatsDataService)->dateDifference($date1, $date2, '%a');
        $this->assertEquals(45, $result);
    }

    public function testPrepareDates()
    {
        $date1 = "2020-06-01";
        $date2 = "null";
        $results = (new StatsDataService)->formatNullDates($date1, $date2);

        $this->assertEquals(null, $results[1]);
        $this->assertEquals($date1, $results[0]);
    }
}
