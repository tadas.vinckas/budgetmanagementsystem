<?php

namespace Tests\Feature\StatsTests;

use Tests\TestCase;
use App\Services\TestService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Laravel\Passport\Passport;
use App\ComponentCategory;

class CategoriesStatsTest extends TestCase
{
   use WithoutMiddleware, RefreshDatabase;

   protected function setUp(): void
   {
      $this->createApplication();

      parent::setUp();
   }

   protected function tearDown(): void
   {
      parent::tearDown();
   }

   public function testCategoriesStatsOnDifferentCategories()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $categories = factory(ComponentCategory::class, 2)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($categories[0]->id, null, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-07-01", $user);
      [$transaction3,] = (new TestService)->createTransaction($categories[0]->id, null, "2020-08-01", $user);
      [$transaction4,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-09-01", $user);
      [$transaction5,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-09-01", $user);

      $sum = $transaction->totalAmount + $transaction2->totalAmount + $transaction3->totalAmount + $transaction4->totalAmount + $transaction5->totalAmount;

      // todo padaryt kitur kaip cia
      $response = $this->get('api/categories-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)->assertJsonFragment([
            [
               "amount" => round($transaction->totalAmount + $transaction3->totalAmount, 2),
               "id" => $categories[0]->id,
               "name" => $categories[0]->name,
               "percentage" => (new TestService)->computePercentage($sum, $transaction->totalAmount + $transaction3->totalAmount)
            ]
         ])
         ->assertJsonFragment([
            [
            "amount" => round($transaction2->totalAmount + $transaction4->totalAmount + $transaction5->totalAmount, 2),
            "id" => $categories[1]->id,
            "name" => $categories[1]->name,
            "percentage" => (new TestService)->computePercentage($sum, $transaction2->totalAmount + $transaction4->totalAmount + $transaction5->totalAmount)
            ]
            ]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
   }

   public function testCategoriesStatsCategoriesSum()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user);

      $response = $this->get('api/categories-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([
         "amount" => round($transaction->totalAmount + $transaction2->totalAmount, 2),
         "percentage" => 100,
         "name" => $category->name
      ]);

      $responseContent = json_decode($response->getContent());

      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
   }

   public function testCategoriesStatsDateIntervalFilterFoundData()
   {
      $dates = array("dateFrom" => "2020-05-22", "dateTo" => "2020-08-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $categories = factory(ComponentCategory::class, 2)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($categories[0]->id, null, "2020-07-01", $user); // included
      [$transaction2,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-10-01", $user);

      $response = $this->get('api/categories-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([[
         "id" => $categories[0]->id,
         "amount" => round($transaction->totalAmount, 2),
         "percentage" =>  100,
         "name" => $categories[0]->name
      ]])->assertJsonFragment([[
         "id" => $categories[1]->id,
         "amount" => 0,
         "percentage" => 0,
         "name" => $categories[1]->name
      ]]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
   }

   public function testCategoriesStatsDateIntervalFilterNotFoundData()
   {
      $dates = array("dateFrom" => "2021-05-22", "dateTo" => "2021-08-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $response = $this->get('api/categories-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0);
   }

   public function testCategoriesStatsAllTimeFoundData()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user); // included

      $response = $this->get('api/categories-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([[
         "id" => $category->id,
         "amount" => $transaction->totalAmount,
         "percentage" => 100,
         "name" => $category->name
      ]]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 1);
   }

   public function testCategoriesStatsAllTimeNotFoundData()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $response = $this->get('api/categories-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0);
   }
}
