<?php

namespace App\Rules;
use App\SavingCampaign;
use Carbon;

use Illuminate\Contracts\Validation\Rule;

class UniqDateSavingCampaign implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $date = Carbon\Carbon::parse($value);

        $month = $date->format('m');
        $year = $date->format('Y');

       $camp = SavingCampaign::ownUser()->where("id", "!=", $this->id)->whereMonth("date","=",$month)->whereYear("date","=", $year)->first();

       if($camp) { 
           return false;
       }

       else { 
          return true;
       }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'saving campaign for this month is already created';
      }
}
