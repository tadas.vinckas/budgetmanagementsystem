<?php

use Illuminate\Http\Request;

/*php
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// user
Route::apiResources(['user' => 'API\UserController']);

// transaction
Route::apiResource('transaction' , 'API\TransactionController');
Route::get('/find-transaction', 'API\TransactionController@search');
Route::post('/transactions', 'API\TransactionController@createTransactionsWithCategoriesSuggestions');
Route::put('transaction/{id}/{componentsToDelete}/{components}' , 'API\TransactionController@update');
Route::get('transactions-categories/{date_from}/{date_to}', 'API\TransactionController@distinctCategories');
Route::get('transaction/beneficiaries/{date_from}/{date_to}/{category_id}' , 'API\TransactionController@distinctBeneficiaries');

// saving campaign
Route::apiResource('saving-campaign', 'API\SavingCampaignController');
Route::get('/saving-campaign/{id}/edit', 'API\SavingCampaignController@edit');
Route::get('/categories', 'API\SavingCampaignController@categories');
Route::get('/saving-campaign/statistics/{id}', 'API\SavingCampaignController@statistics');

// component category
Route::apiResource('component-category', 'API\ComponentCategoryController');
Route::post('sort-categories' , 'API\ComponentCategoryController@sortCategories');
Route::get('category-beneficiaries/{category_id}', 'API\ComponentCategoryController@categoryBeneficiaries');

// statement
Route::post('/statement/import', 'API\StatementsController@importStatement');
Route::get('/imported-transactions/{id}', 'API\StatementsController@readImportedTransactions');
Route::get('/statements', 'API\StatementsController@getStatements');
Route::delete('statements/{id}/{processed}', 'API\StatementsController@delete');

// saving campaign
Route::get('/saving-campaigns-charts-stats/{date_from}/{date_to}', 'API\StatsController@savingCampaignsChartsStats');
Route::get('saving-campaigns-category-stats/{category_id}/{date_from}/{date_to}', 'API\StatsController@savingCampaignsCategoryStats');
Route::get('saving-campaigns-total-stats/{date_from}/{date_to}', 'API\StatsController@savingCampaignsTotalStats');
Route::get('saving-campaigns-categories-stats/{date_from}/{date_to}', 'API\StatsController@savingCampaignsCategoriesStats');

// stats / charts
Route::get('/total-stats/{date_from}/{date_to}', 'API\StatsController@totalStats');
Route::get('/category-stats/{category_id}/{date_from}/{date_to}', 'API\StatsController@categoryStats');
Route::get('/category-beneficiaries-stats/{category_id}/{date_from}/{date_to}', 'API\StatsController@categoryBeneficiariesStats');
Route::get('/category-beneficiary-stats/{beneficiary_id}/{category_id}/{date_from}/{date_to}', 'API\StatsController@categoryBeneficiaryStats');
Route::get('/beneficiary-stats/{beneficiary_id}/{date_from}/{date_to}', 'API\StatsController@beneficiaryStats');
Route::get('/categories-stats/{date_from}/{date_to}', 'API\StatsController@categoriesStats');
Route::get('/main-charts-stats/{date_from}/{date_to}', 'API\StatsController@mainChartsStats');

// report
Route::get('/report/{date}', 'API\ReportController@report');
Route::get('/report-summary/{date}', 'API\ReportController@reportSummary');

Route::get('profile', 'API\UserController@profile');
// Route::get('findUser', 'API\UserController@search');
Route::put('profile', 'API\UserController@updateProfile');