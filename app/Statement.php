<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $fillable = ['name', 'user_id'];

    public function scopeOwnUser($query)
    {
        return $query->where('user_id',  auth('api')->user()->id);
    }
}
