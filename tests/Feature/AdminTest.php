<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Laravel\Passport\Passport;
use App\User;
use App\ComponentCategory;
use App\Services\TestService;

class AdminTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;

    protected function setUp(): void
    {
        $this->createApplication();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testCreateUser() { 

        $user = factory(User::class)->create();
        $user->type = "admin";
        $user->save();
        Passport::actingAs($user);


        $data = [
            "bio" => "test",
            "email" => "test1@gmail.com",
            "password" => "testpasword",
            "type" => "client",
            "name" => "tadas"
        ];

        $this->post('/api/user', $data)->assertStatus(201);

        $createdUser = User::where("email", $data["email"])->first();
        $this->assertNotNull($createdUser);
        $this->assertEquals($createdUser->email, $data["email"]);
        $this->assertEquals($createdUser->type, $data["type"]);
        $this->assertEquals($createdUser->bio, $data["bio"]);
    }

    public function testUserUpdate() { 

        $user = factory(User::class)->create();
        $user->save();

        Passport::actingAs($user);

        $userNew = factory(User::class)->create();
        $userNew->save();

        $data = [
            "bio" => "test",
            "email" => "test1@gmail.com",
            "password" => "testpasword",
            "type" => "client",
            "name" => "tadas"
        ];

        $this->put('/api/user/'. $userNew->id , $data)->assertStatus(200);

        $updatedUser = User::where("email", $data["email"])->first();
        $this->assertNotNull($updatedUser);
        $this->assertEquals($updatedUser->email, $data["email"]);
        $this->assertEquals($updatedUser->type, $data["type"]);
        $this->assertEquals($updatedUser->bio, $data["bio"]);
    }

    public function testCreateUserAsClient() { 

        $user = factory(User::class)->create();
        $user->type = "client";
        $user->save();
        Passport::actingAs($user);

        $data = [
            "bio" => "test",
            "email" => "test1@gmail.com",
            "password" => "testpasword",
            "type" => "client",
            "name" => "tadas"
        ];

        $this->post('/api/user', $data)->assertStatus(403);
    }

    public function testUserUpdateAsClient() { 

        $user = factory(User::class)->create();
        $user->type = "client";
        $user->save();

        Passport::actingAs($user);

        $userNew = factory(User::class)->create();

        $data = [
            "bio" => "test",
            "email" => "test1@gmail.com",
            "password" => "testpasword",
            "type" => "client",
            "name" => "tadas"
        ];

        $this->put('/api/user/'. $userNew->id , $data)->assertStatus(403);
    }

    public function testUserDelete() { 

        $user = factory(User::class)->create();
        $user->save();

        $userNew = factory(User::class)->create();
        Passport::actingAs($user);

        $userNew = factory(User::class)->create();
        $this->delete('/api/user/'. $userNew->id)->assertStatus(200);
    }

    public function testUserDeleteAsClient() {
        $user = factory(User::class)->create();
        $user->type = "client";
        $user->save();

        $userNew = factory(User::class)->create();
        Passport::actingAs($user);

        $userNew = factory(User::class)->create();
        $this->delete('/api/user/'. $userNew->id)->assertStatus(403);
    }
}