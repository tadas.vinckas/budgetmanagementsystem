<?php

namespace App\Http\Requests\ComponentCategoryControllerRequests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\UniqCategory;
use Route;

class ComponentCategoryUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:191', new UniqCategory]
        ];
    }
}
