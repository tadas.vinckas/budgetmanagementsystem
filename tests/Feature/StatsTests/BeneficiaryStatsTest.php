<?php

namespace Tests\Feature\StatsTests;

use Tests\TestCase;
use App\Services\TestService;
use App\Services\StatsCalcService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Laravel\Passport\Passport;
use App\Transaction;
use App\CustomBeneficiary;
use App\ComponentCategory;

class BeneficiaryStatsTest extends TestCase
{
   use WithoutMiddleware, RefreshDatabase;

   protected function setUp(): void
   {
      $this->createApplication();

      parent::setUp();
   }

   protected function tearDown(): void
   {
      parent::tearDown();
   }

   public function testBeneficiaryStatsOnDifferentBeneficiaries()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction(null, $customBeneficiary->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction(null, null, "2020-07-01", $user);
      [$transaction3,] = (new TestService)->createTransaction(null, null, "2020-08-01", $user);
      [$transaction4,] = (new TestService)->createTransaction(null, $customBeneficiary->id, "2020-09-01", $user);
      [$transaction5,] = (new TestService)->createTransaction(null, null, "2020-10-01", $user);

      $response = $this->get('api/beneficiary-stats/' . $customBeneficiary->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([[
            "date" => "2020-09",
            "sum" => round($transaction4->totalAmount, 2),
         ]])->assertJsonFragment([[
            "date" => "2020-08",
            "sum" => 0
         ]])->assertJsonFragment([[
            "date" => "2020-07",
            "sum" => round($transaction->totalAmount,2)
         ]]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 3); // includes 3 months
   }

   public function testBeneficiaryStatsOnSameMonth()
   {

      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction(null, $customBeneficiary->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction(null, $customBeneficiary->id, "2020-07-01", $user);

      $response = $this->get('api/beneficiary-stats/' . $customBeneficiary->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([[
         "date" => "2020-07",
         "sum" => round($transaction->totalAmount + $transaction2->totalAmount, 2)
      ]]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
      $this->assertEquals(count($response), 1); // includes 1 month
   }

   public function testBeneficiaryStatsMonthsInclusionFoundData()
   {
      $dates = array("dateFrom" => "2020-05-22", "dateTo" => "2020-09-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction(null, $customBeneficiary, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction(null, $customBeneficiary, "2020-10-01", $user);

      $response = $this->get('api/beneficiary-stats/' . $customBeneficiary->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([[
         "date" => "2020-07",
         "sum" => round($transaction->totalAmount,2)
      ]]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 1); // includes 1 month
   }

   public function testBeneficiaryStatsMonthsInclusionNotFoundData()
   {
      $dates = array("dateFrom" => "2020-11-22", "dateTo" => "2020-12-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction(null, $customBeneficiary, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction(null, $customBeneficiary, "2020-10-01", $user);

      $response = $this->get('api/beneficiary-stats/' . $customBeneficiary->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);
     
      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0); // includes 0 months
   }

   public function testBeneficiaryStatsOnAllTimeIntervalFoundData()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction(null, $customBeneficiary, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction(null, $customBeneficiary, "2020-09-01", $user);

      $response = $this->get('api/beneficiary-stats/' . $customBeneficiary->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([[
         "sum" => $transaction2->totalAmount,
         "date" => "2020-09",
      ]])->assertJsonFragment([[
         "date" => "2020-08",
         "sum" => 0
      ]])->assertJsonFragment([[
         "date" => "2020-07",
         "sum" => $transaction->totalAmount
      ]]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 3); // includes 3 months
   }

   public function testBeneficiaryStatsOnAllTimeIntervalNotFoundData()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      $response = $this->get('api/beneficiary-stats/' . $customBeneficiary->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0); // includes 0 months
   }
}
