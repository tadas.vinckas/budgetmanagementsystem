<?php

namespace App\Services;

use App\ComponentCategory;
use App\Services\StatsCalcService;
use App\Transaction;
use Carbon;

class StatsDataService
{
    function dateDifference($date_1, $date_2, $differenceFormat = '%a')
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);

        $interval = date_diff($datetime1, $datetime2);

        return $interval->format($differenceFormat);
    }
    
    public function categoryBeneficiariesWithAmountsSums($categoryId, $beneficiaryId, $dateFrom, $dateTo)
    {
        $components = $this->categoryOrBeneficiaryTransactions($categoryId, $beneficiaryId, $dateFrom, $dateTo,null)->get();

        $results = array();
        $foundBen = false;

        foreach ($components as $component) {
            foreach ($results as &$result) {
                if ($result["beneficiary"] == $component->beneficiary) {
                    $result["amount"] = (new StatsCalcService)->add((float) $result["amount"],(float)$component->amount);
                    $foundBen = true;
                }
            }
            if (!$foundBen) {
                array_push($results, ["beneficiary" => $component->beneficiary, "beneficiary_name" => $component->beneficiary_name,"amount" => $component->amount]);
                $foundBen = false;
            }
        }

        return $results;
    }

    public function categoryOrBeneficiaryTransactions($categoryId, $beneficiaryId, $dateFrom, $dateTo, $monthDate)
    {
        $transactions = Transaction::where('transactions.user_id', auth('api')->user()->id)->processed(1);

        if ($dateFrom && $dateTo) {
            $transactions->whereBetween('date', [$dateFrom, $dateTo]);
        }

        if ($monthDate) {
            $transactions->whereYear('date', $monthDate["year"])->whereMonth('date', $monthDate["month"]);
        }

        $transactions->leftJoin('transaction_components', 'transactions.id', '=', 'transaction_components.transaction_id');

        $transactions->leftJoin('custom_beneficiaries', 'beneficiary', '=', 'custom_beneficiaries.id');

        $transactions->select('date', 'custom_beneficiaries.name as beneficiary_name', 'transactions.beneficiary as beneficiary', 'transaction_components.category_id', 'transaction_components.amount as amount');

        if ($beneficiaryId) {
            $transactions->where('beneficiary', $beneficiaryId);
        }

        if ($categoryId) {
            $transactions->where('category_id', $categoryId);
        }

        return $transactions;
    }


    public function getDistinctBeneficiaries($dateFrom, $dateTo, $category)
    {
        $results = $this->categoryOrBeneficiaryTransactions($category, null, $dateFrom, $dateTo, null);
        $results = $results->pluck('beneficiary','beneficiary_name');
        return $results;
    }

    public function getDistinctCategories($dateFrom, $dateTo)
    {

        $results = $this->categoryOrBeneficiaryTransactions(null, null, $dateFrom, $dateTo,null);

        $results = $results->pluck('transactionComponent.category_id')->unique();

        $categories = ComponentCategory::ownUser()->whereIn("id", $results)->orderBy("position")->pluck("id","name");

        return $categories;
    }

    public function prepareDates($dateFrom, $dateTo)
    {

        $date = $dateFrom;
        $dates = array();
        $stats_date = array("year" => "", "month" => "", "amount" => 0, "label" => "");

        while (strtotime($date) <= strtotime($dateTo)) {

            $old_date = $date;

            $date = date("Y-m", strtotime("+1 month", strtotime($date)));

            $year = date("Y", strtotime($old_date));
            $month = date("m", strtotime($old_date));

            $stats_date["year"] = $year;
            $stats_date["month"] = $month;
            $stats_date["label"] = $year . "-" . $month;

            array_push($dates, $stats_date);
        }
        return $dates;
    }

    public function getDatesForSavingCampChartsStats($dateFrom, $dateTo) { 

        if ($dateFrom != "null" && $dateTo != "null") {
             
            $dateFrom = \DateTime::createFromFormat('D M d Y H:i:s e+', $dateFrom)->format('Y-m-d');
            $dateTo = \DateTime::createFromFormat('D M d Y H:i:s e+', $dateTo)->format('Y-m-d');
    
        } else {
            $dateFrom = Transaction::ownUser()->min('date');
            $dateTo = Transaction::ownUser()->max('date');
        }

        return [$dateFrom, $dateTo];
    }

    public function prepareResultsDataSetForSavCamp($categories)
    {

        $categoriesStats = array();

        foreach ($categories as &$cat) {

            $categoryStats = array(
                "name" => $cat->name,
                "amount" => 0,
                "percentage" => 0,
                "id" => $cat->id
            );

            array_push($categoriesStats, $categoryStats);
        }

        return $categoriesStats;
    }

    public function formatNullDates($dateFrom, $dateTo) { 
        $dateFrom = $dateFrom == "null" ? null : $dateFrom;
        $dateTo = $dateTo == "null" ? null : $dateTo;
        return [$dateFrom, $dateTo];
    }
}
