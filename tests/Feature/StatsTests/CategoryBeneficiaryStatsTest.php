<?php

namespace Tests\Feature\StatsTests;

use Tests\TestCase;
use App\Services\TestService;
use App\Services\StatsCalcService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Laravel\Passport\Passport;
use App\Transaction;
use App\CustomBeneficiary;
use App\ComponentCategory;

class CategoryBeneficiaryStatsTest extends TestCase
{
   use WithoutMiddleware, RefreshDatabase;

   protected function setUp(): void
   {
      $this->createApplication();

      parent::setUp();
   }

   protected function tearDown(): void
   {
      parent::tearDown();
   }

   public function testCategoryBeneficiaryStatsOnDifferentCategoriesAndBeneficiaries()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $categories = factory(ComponentCategory::class, 2)->create([
        "user_id" => $user->id,
     ]);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary->id, "2020-07-01", $user); // include
      [$transaction2,] = (new TestService)->createTransaction($categories[0]->id, null, "2020-07-01", $user);
      [$transaction3,] = (new TestService)->createTransaction(null, null, "2020-08-01", $user);

      $response = $this->get('api/category-beneficiary-stats/' . $customBeneficiary->id . '/' . $categories[0]->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
         ->assertJsonFragment([
            "date" => "2020-07",
            "sum" => $transaction->totalAmount
         ]
         );

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 1); // includes one month
   }

   public function testCategoryBeneficiaryStatsOnSameMonth()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
        "user_id" => $user->id,
     ]);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user); // include
      [$transaction2,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user); // include

      $response = $this->get('api/category-beneficiary-stats/' . $customBeneficiary->id . '/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([
         "date" => "2020-07",
         "sum" =>  round($transaction->totalAmount + $transaction2->totalAmount,2)
      ]
      );

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 1); // includes one month
   }

   public function testCategoryBeneficiaryStatsMonthsInclusionFoundData()
   {
      $dates = array("dateFrom" => "2020-05-22", "dateTo" => "2020-09-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
        "user_id" => $user->id,
     ]);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user); // include
      [$transaction2,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-10-01", $user);

      $response = $this->get('api/category-beneficiary-stats/' . $customBeneficiary->id . '/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
      ->assertJsonFragment([
         "date" => "2020-07",
         "sum" =>   $transaction->totalAmount
      ]
      );

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 1); // includes one month
   }

   public function testCategoryBeneficiaryStatsMonthsInclusionNotFoundData()
   {
      $dates = array("dateFrom" => "2021-05-22", "dateTo" => "2021-09-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
        "user_id" => $user->id,
     ]);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-10-01", $user);

      $response = $this->get('api/category-beneficiary-stats/' . $customBeneficiary->id . '/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);
     
      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0); // includes 0 months
   }

   public function testCategoryBeneficiaryStatsOnAllTimeFoundData()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
        "user_id" => $user->id,
     ]);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user); // include

      $response = $this->get('api/category-beneficiary-stats/' . $customBeneficiary->id . '/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);

      $this->assertEquals(count($response), 1); // includes one month
   }

   public function testCategoryBeneficiaryStatsOnAllTimeNotFoundData()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
        "user_id" => $user->id,
     ]);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      $response = $this->get('api/category-beneficiary-stats/' . $customBeneficiary->id . '/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0); // includes 0 months
   }
}
