
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


window.Vue = require('vue');

import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.user);

import DatePicker from 'vue2-datepicker';
import 'vue2-datepicker/index.css';
Vue.use(DatePicker);

import VueMonthlyPicker from 'vue-monthly-picker';
Vue.use(VueMonthlyPicker);

import swal from 'sweetalert2'
window.swal = swal;

import { Doughnut, Line, Bar, HorizontalBar} from 'vue-chartjs';
Vue.use(Doughnut);
Vue.use(Line);
Vue.use(Bar);

import ChartDataLabels from 'chartjs-plugin-datalabels';
Vue.use(ChartDataLabels);

import AllIosIcon from 'vue-ionicons/dist/ionicons-ios.js'
Vue.use(AllIosIcon)

import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

import vuedraggable from 'vuedraggable'
Vue.use(vuedraggable)

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
 

//global registration
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
Vue.use(VueFormWizard)

require('vue-ionicons/ionicons.css')


import jsPDF from 'jspdf';
import 'jspdf-autotable';

const toast = swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

window.toast = toast;


window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

Vue.component('pagination', require('laravel-vue-pagination'));

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '3px'
  })

let routes = [
    // { path: '/dashboard', component: require('./components/Dashboard.vue') },
    { path: '/developer', component: require('./components/Developer.vue') },
    { path: '/users', component: require('./components/Users.vue') },
    { path: '/profile', component: require('./components/Profile.vue') },
    { path: '/dashboard', component: require('./components/Dashboard.vue') },
    { path: '/components-categories', component: require('./components/ComponentsCategories.vue') },
    { path: '/charts', component: require('./components/Charts.vue') },
    { path: '/report', component: require('./components/Report.vue') },
    { path: '/save-money', component: require('./components/SaveMoney.vue') },
    { path: '*', component: require('./components/NotFound.vue') }
  ]

const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
  })

Vue.filter('upText', function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});

Vue.filter('myDate',function(created){
    return moment(created).format('MM/DD/YYYY')
});


window.Fire =  new Vue();

window.Fire.getRandomColorHex = function(length) {
    var hex = "0123456789ABCDEF";
    var colors = [];

    for (var j = 0; j < length; j++) {
      colors[j] = "#";
      for (var i = 1; i <= 6; i++) {
        colors[j] += hex[Math.floor(Math.random() * 16)];
      }
    }
    return colors;
}

window.Fire.printme = function() { 
    
    var printContents = document.getElementById("stats").innerHTML;
    var originalContents = document.body.innerHTML;

    document.body.innerHTML = printContents;

    window.print();

    document.body.innerHTML = originalContents;
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue')
);


Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app',
    router,
    data:{
        search: ''
    },

    methods:{
        searchit: _.debounce(() => {
            Fire.$emit('searching');
        },1000),
    }
});
