<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\StatsDataService;
use App\Services\StatsCalcService;
use App\ComponentCategory;

class StatsController extends Controller
{

    public function __construct()
    {
        $this->statsCalcService = new StatsCalcService;
        $this->statsDataService = new StatsDataService;
        $this->middleware('auth:api');
    }

    public function totalStats(Request $request, $dateFrom, $dateTo)
    {
        return $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, null, null, $request->paginate, true);
    }

    public function categoryStats(Request $request, $categoryId, $dateFrom, $dateTo)
    {

        return $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, $categoryId, null, $request->paginate, true);
    }

    public function beneficiaryStats(Request $request, $beneficiaryId, $dateFrom, $dateTo)
    {
        return $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, null, $beneficiaryId, $request->paginate, true);
    }

    public function categoryBeneficiaryStats(Request $request, $beneficiaryId, $categoryId, $dateFrom, $dateTo)
    {
        return $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, $categoryId, $beneficiaryId, $request->paginate, true);
    }

    public function categoriesStats(Request $request, $dateFrom, $dateTo)
    {
        return $this->statsCalcService->getCategoriesStatsData($dateFrom, $dateTo, true, $request->paginate, true);
    }

    public function categoryBeneficiariesStats(Request $request, $categoryId, $dateFrom, $dateTo)
    {
        return $this->statsCalcService->getCategoryBeneficiariesStatsData($dateFrom, $dateTo, true, $categoryId, $request->paginate, true);
    }

    public function savingCampaignsCategoryStats(Request $request, $categoryId, $dateFrom, $dateTo)
    {

        $results = $this->statsCalcService->getSavingCampaignsChartsStatsData($dateFrom, $dateTo, $request->paginate ? "category" : null, $categoryId, "months");

        return $request->paginate == 1 ? $results : $results["categoryStats"];
    }

    public function savingCampaignsTotalStats(Request $request, $dateFrom, $dateTo)
    {
        $results = $this->statsCalcService->getSavingCampaignsChartsStatsData($dateFrom, $dateTo, $request->paginate ? "total" : null, null, "months");

        return $request->paginate == 1 ? $results : $results["totalStats"];
    }

    public function savingCampaignsCategoriesStats(Request $request, $dateFrom, $dateTo)
    {
        $results = $this->statsCalcService->getSavingCampaignsChartsStatsData($dateFrom, $dateTo, $request->paginate ? "categories" : null, null, "categories");
        
        return $request->paginate == 1 ? $results : $results["categoriesStats"];
    }

    public function savingCampaignsChartsStats($dateFrom, $dateTo)
    {
        $categories = ComponentCategory::ownUser()->orderBy("position")->get();

        if (count($categories) == 0) {
            return ["dataNotFound" => true];
        }

        $stats = $this->statsCalcService->getSavingCampaignsChartsStatsData($dateFrom, $dateTo, null, $categories[0]["id"], "all");

        $areChartsEmpty = $this->statsCalcService->checkIfChartsAreEmpty($stats["categoriesStats"]);

        return [
            "categoriesStats" => $stats["categoriesStats"],
            "totalStats" => $stats["totalStats"],
            "categoryStats" => $stats["categoryStats"],
            "categories" => $categories,
            "dataNotFound" => false,
            "areChartsEmpty" => $areChartsEmpty
        ];
    }

    public function mainChartsStats($dateFrom, $dateTo)
    {

        [$dateFrom, $dateTo] = $this->statsDataService->formatNullDates($dateFrom, $dateTo);

        $categories = $this->statsDataService->getDistinctCategories(
            $dateFrom,
            $dateTo,
            null
        );

        if ($categories->count() <= 0) {
            return ["dataNotFound" => true];
        }

        $categoryBeneficiaries = $this->statsDataService->getDistinctBeneficiaries($dateFrom, $dateTo, $categories->first(), null);
        $beneficiaries = $this->statsDataService->getDistinctBeneficiaries($dateFrom, $dateTo, null, null);

        return [
            "totalStats" => $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, null, null,  null, false),
            "categoryStats" =>  $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, $categories->first(), null, null, false),
            "beneficiaryStats" => $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, null, $categoryBeneficiaries->first(), null, false),
            "categoryBeneficiaryStats" => $this->statsCalcService->getMonthsStatsData($dateFrom, $dateTo, $categories->first(), $categoryBeneficiaries->first(), null, false),
            "categoryBeneficiaries" => $categoryBeneficiaries,
            "transactionsCategories" => $categories,
            "beneficiaries" => $beneficiaries,
            "dataNotFound" => false,
            "categoriesStats" => $this->statsCalcService->getCategoriesStatsData($dateFrom, $dateTo, true, null, false),
            "categoryBeneficiariesStats" => $this->statsCalcService->getCategoryBeneficiariesStatsData($dateFrom, $dateTo, true, $categories->first(), null, false)
        ];
    }
}
