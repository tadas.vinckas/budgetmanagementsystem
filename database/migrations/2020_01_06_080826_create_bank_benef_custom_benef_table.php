<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBankBenefCustomBenefTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_benef_custom_benef', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_beneficiary_id')->unsigned()->index()->nullable();
            $table->foreign('bank_beneficiary_id')->references('id')->on('bank_beneficiaries');
            $table->integer('custom_beneficiary_id')->unsigned()->index()->nullable();
            $table->foreign('custom_beneficiary_id')->references('id')->on('custom_beneficiaries');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_benef_custom_benef');
    }
}
