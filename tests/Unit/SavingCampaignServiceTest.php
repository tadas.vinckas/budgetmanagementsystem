<?php 

namespace Tests\Unit;
use Tests\TestCase;
use Carbon\Carbon;
use App\Services\SavingCampaignService;

class SavingCampaignServiceTest extends TestCase 
{
    public function testPrepareFirstAndLastMonthDates()
    {
        $date = "2020-08-14";
        $dates = (new SavingCampaignService)->prepareFirstAndLastMonthDates($date);
        $this->assertEquals($dates["first"], "2020-08-01");
        $this->assertEquals($dates["last"], "2020-08-31");
    }

     /**
     * @dataProvider activeStatusDataProvider
     */
    public function testGetActiveStatus($data) { 
        $status = (new SavingCampaignService)->getActiveStatus($data["date"]);
        $this->assertEquals($status, $data["status"]);
    }


    public function activeStatusDataProvider()
    {
        return [
            '0' => [[
                'date' => Carbon::now()->subYear(),
                'status' => 0
            ]],
            '1' => [[
                'date' => Carbon::now(),
                'status' => 1
            ]],
            '2' => [[
                'date' => Carbon::now()->addYear(),
                'status' => 2
            ]],
        ];
    }
}