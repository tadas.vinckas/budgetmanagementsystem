<?php

namespace App\Services;

use App\CustomBeneficiary;
use App\BankBeneficiary;
use App\User;
use App\Transaction;

class BeneficiaryService
{

    public function processBeneficiaryName($value, $firstWord)
    {

        if (strlen($value) > 3) { // if UAB LT, dont change
            $value = mb_strtolower(strval($value), 'UTF-8');
            $value = ucfirst($value);
        }
        return $firstWord ? $value : " " . $value;
    }

    public function convertBeneficiaryName($benef, $isCustomBeneficiary, $benefLength)
    {
        $pieces = explode(" ", $benef);
        $convertedName = "";
        $firstWord = true;

        foreach ($pieces as $p) {
            if (strlen($benef) >= $benefLength) {

                $digitsCount = preg_match_all("/[0-9]/", $p);
                $lettersCount = preg_match_all('/\p{L}/', $p);

                // if is bank beneficiary and bad word
                if (!$isCustomBeneficiary && $lettersCount >= 1 && $digitsCount >= 3) {
                    continue;
                }

                // if is custom beneficiary and bad word 
                else if ($isCustomBeneficiary && ($lettersCount <= 3 || $digitsCount > 0)) {
                    continue;
                } else {
                    $convertedName .= $this->processBeneficiaryName($p, $firstWord);
                    $firstWord = false;
                }
            } else {
                $convertedName .= $this->processBeneficiaryName($p, $firstWord);
                $firstWord = false;
            }
        }

        return $convertedName;
    }

    /** get custom beneficiary for bank beneficiary */
    public function getBankBeneficiaryCustomBeneficiary($benef, $bankBenef)
    {
        $customBeneficiaries = User::find(auth('api')->user()->id)->customBeneficiaries; // todo

        foreach ($customBeneficiaries as $customBenef) {

            // if found custom beneficiary having relation with bank beneficiary which has name $benef
            if ($customBenef->bankBeneficiaries->contains("name", $benef)) {

                return $customBenef;
            }
        }

        // get custom beneficiary
        $customBenef = $this->getCustomBeneficiaryOnRelatedToBankBeneficiaries($benef);
        $customBenef->bankBeneficiaries()->attach($bankBenef->id);

        return $customBenef;
    }


    public function handleBankBeneficiary($benef)
    {
        $benef = $this->convertBeneficiaryName($benef, false, 4);

        $bankBenef = BankBeneficiary::where('name', $benef)->first();

        // if exists bank beneficiary with that name
        if ($bankBenef) {

            $customBenef = $this->getBankBeneficiaryCustomBeneficiary($benef, $bankBenef);
        } else {

            $customBenef = $this->getCustomBeneficiaryOnRelatedToBankBeneficiaries($benef);

            // create bank beneficiary
            $bankBenef = BankBeneficiary::create([
                "name" => $benef
            ]);

            // attach to custom beneficiary bank relations
            $customBenef->bankBeneficiaries()->attach($bankBenef->id);
        }
        return $customBenef->id;
    }

    public function getCustomBeneficiaryOnRelatedToBankBeneficiaries($benef)
    {
        // find related bank custom beneficiary with same name
        $relatedToBankCustomBenef = CustomBeneficiary::ownUser()->where("name", $benef)->relatedToBank(1)->first();

        if ($relatedToBankCustomBenef) {

            $customBenef = CustomBeneficiary::ownUser()->find($relatedToBankCustomBenef->id);
        } else {
            // create custom beneficiary
            $customBenef = CustomBeneficiary::create([
                "name" => $benef,
                "user_id" => auth('api')->user()->id,
                "relatedToBank" => 1
            ]);
        }

        return $customBenef;
    }

    /** checks if exists beneficiary from not related to bank beneficiaries with same name
     * and if true, update transaction, with same name beneficiary name, id to new beneficiary_id and delete old beneficiary.
     * Update beneficiary name to new.
      */
    public function processNotRelatedToBankBeneficiaries($beneficiary, $beneficiary_id)
    {

        // search for beneficiary with same name
        $notRelatedToBankCustomBenef = CustomBeneficiary::ownUser()->where("name", $beneficiary)->relatedToBank(0)->first();

        // if beneficiary with same name exists
        if ($notRelatedToBankCustomBenef) {

             // update beneficiary with found same name beneficiary id
            Transaction::ownUser()->where("beneficiary", $notRelatedToBankCustomBenef->id)->update(["beneficiary" => $beneficiary_id]);

            // delete old beneficiary 
            CustomBeneficiary::ownUser()->find($notRelatedToBankCustomBenef->id)->delete();
        }

        // update beneficiary name
        CustomBeneficiary::ownUser()->find($beneficiary_id)->update(["name" => $beneficiary]);
    }

    public function processRelatedToBankBeneficiaries($beneficiary, $beneficiary_id)
    {
        // search for beneficiary with same name
        $relatedToBankCustomBenef = CustomBeneficiary::ownUser()->relatedToBank(1)->where("id", "!=", $beneficiary_id)->where("name", $beneficiary)->first();

        // if beneficiary with same name exists
        if ($relatedToBankCustomBenef) {

            // update beneficiary with found same name beneficiary id
            Transaction::ownUser()->where("beneficiary", $beneficiary_id)->update(["beneficiary" => $relatedToBankCustomBenef->id]);

            // attach all old custom beneficiary bank relations with new beneficiary
            $pastBeneficiaries = CustomBeneficiary::ownUser()->find($beneficiary_id);
            $pastBeneficiariesBankBenef = $pastBeneficiaries->bankBeneficiaries->pluck("pivot.bank_beneficiary_id");
            $relatedToBankCustomBenef->bankBeneficiaries()->attach($pastBeneficiariesBankBenef);

            // delete beneficiary with its bank connctions
            $pastBeneficiaries->bankBeneficiaries()->detach();
            $pastBeneficiaries->delete();

            // return new id
            return $relatedToBankCustomBenef->id;
        } else {
            return $beneficiary_id;
        }
    }

    public function getTransactionBeneficiary($beneficiary_id, $beneficiary)
    {
        if ($beneficiary_id) {
            $beneficiary_id = $this->processRelatedToBankBeneficiaries($beneficiary, $beneficiary_id);
        }

        $this->processNotRelatedToBankBeneficiaries($beneficiary, $beneficiary_id);

        return $beneficiary_id;
    }

    /** get custom beneficiary for new beneficiary name */
    public function getTransactionBeneficiaryInStore($beneficiary)
    {

        $customBeneficiary =  CustomBeneficiary::ownUser()->where("name", $beneficiary)->first();

        // if beneficiary exists with that name return it
        if ($customBeneficiary) {
            return $customBeneficiary->id;
        } else {
            // create new beneficiary
            $customBeneficiary = CustomBeneficiary::create([
                "name" => $beneficiary,
                "user_id" => auth('api')->user()->id
            ]);

            return $customBeneficiary->id;
        }
    }
    
    public function getTransactionBeneficiaryInUpdate($beneficiary_id, $beneficiary)
    {
        $customBeneficiary = CustomBeneficiary::ownUser()->find($beneficiary_id);

        // if custom beneficiary name hasn't changed
        if (isset($customBeneficiary) && $customBeneficiary->name == $beneficiary) {

            return $beneficiary_id;
        } else {
            return $this->getTransactionBeneficiary($beneficiary_id, $beneficiary);
        }
    }
}
