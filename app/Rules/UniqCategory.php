<?php

namespace App\Rules;
use App\ComponentCategory;

use Illuminate\Contracts\Validation\Rule;

class UniqCategory implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
       $category = ComponentCategory::ownUser()->where('user_id', auth('api')->user()->id)->where('name',$value)->first();
   
       if($category) { 
           return false;
       }

       else { 
          return true;
       }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'this category is already created';
      }
}
