<?php

namespace Tests\Feature\StatsTests;

use Tests\TestCase;
use App\Services\TestService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Laravel\Passport\Passport;
use App\CustomBeneficiary;
use App\ComponentCategory;

class TotalStatsTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;

    protected function setUp(): void
    {
        $this->createApplication();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testTotalStatsOnDifferentCategories()
    {
        $dates = array("dateFrom" => "null", "dateTo" => "null");

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $categories = factory(ComponentCategory::class, 2)->create([
            "user_id" => $user->id,
        ]);

        [$transaction,] = (new TestService)->createTransaction($categories[0]->id, null, "2020-07-01", $user);
        [$transaction2,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-08-01", $user);

        $response = $this->get('api/total-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
        ->assertJsonFragment([[
            "date" => "2020-07",
            "sum" =>  round($transaction->totalAmount,2)
        ]])->assertJsonFragment([[
                "date" => "2020-08",
                "sum" =>  round($transaction2->totalAmount,2)
         ]]);

         $responseContent = json_decode($response->getContent());
         $response = $responseContent->results->data;
   
         $this->assertEquals($responseContent->foundData, true);
   
         $this->assertEquals(count($response), 2); // includes 2 months
    }

    public function testTotalStatsOnDifferentCategoriesOnSameMonth()
    {
        $dates = array("dateFrom" => "null", "dateTo" => "null");

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id,
        ]);

        [$transaction,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user); // include
        [$transaction2,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user); // include

        $response = $this->get('api/total-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
        ->assertJsonFragment([
            "date" => "2020-07",
            "sum" => round($transaction->totalAmount+$transaction2->totalAmount,2)
        ]);

        $responseContent = json_decode($response->getContent());
        $response = $responseContent->results->data;
  
        $this->assertEquals($responseContent->foundData, true);
  
        $this->assertEquals(count($response), 1); // includes 1 month
    }

    public function testTotalStatsMonthsInclusionFoundData()
    {
        $dates = array("dateFrom" => "2020-05-22", "dateTo" => "2020-09-22");

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        [$transaction,] = (new TestService)->createTransaction(null, null, "2020-07-01", $user); // include
        [$transaction2,] = (new TestService)->createTransaction(null, null, "2020-10-01", $user); // not include

        $response = $this->get('api/total-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
        ->assertJsonFragment([
            "date" => "2020-07",
            "sum" => round($transaction->totalAmount,2)
        ]);

        $responseContent = json_decode($response->getContent());
        $response = $responseContent->results->data;
  
        $this->assertEquals($responseContent->foundData, true);
  
        $this->assertEquals(count($response), 1); // includes 1 month
    }

    public function testTotalStatsMonthsInclusionNotFoundData()
    {
        $dates = array("dateFrom" => "2021-05-22", "dateTo" => "2021-09-22");

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id,
        ]);

        [$transaction,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user);

        $response = $this->get('api/total-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

        $responseContent = json_decode($response->getContent());
        $response = $responseContent->results->data;
   
        $this->assertEquals($responseContent->foundData, false);
   
        $this->assertEquals(count($response), 0); // includes 0 month
    }

    public function testCategoryStatsOnAllTimeFoundData()
    {
        $dates = array("dateFrom" => "null", "dateTo" => "null");

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id,
        ]);

        [$transaction,] = (new TestService)->createTransaction($category->id, null, "2020-07-01", $user);

        $response = $this->get('api/total-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
        ->assertJsonFragment([
            "date" => "2020-07",
            "sum" => round($transaction->totalAmount,2)
        ]);

        $responseContent = json_decode($response->getContent());
        $response = $responseContent->results->data;
  
        $this->assertEquals($responseContent->foundData, true);
  
        $this->assertEquals(count($response), 1); // includes 1 month
    }
    
    public function testCategoryStatsOnAllTimeNotFoundData()
    {
        $dates = array("dateFrom" => "null", "dateTo" => "null");

        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $category = factory(ComponentCategory::class)->create([
            "user_id" => $user->id,
        ]);

        $response = $this->get('api/total-stats/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

         $responseContent = json_decode($response->getContent());
         $response = $responseContent->results->data;
   
         $this->assertEquals($responseContent->foundData, false);
   
         $this->assertEquals(count($response), 0); // includes 0 months
    }
}
