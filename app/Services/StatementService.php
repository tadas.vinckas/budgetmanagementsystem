<?php

namespace App\Services;

use App\ComponentCategory;
use App\Services\BeneficiaryService;
use App\Transaction;
use App\Statement;
use App\TransactionComponent;
use Illuminate\Support\Collection;
use Carbon;
use Validator;
use Excel;
use Storage;

class StatementService
{
    public function createTransactionFromFile($value, $statement_id)
    {
        $beneficiaryService = new BeneficiaryService;

        // if exists beneficiary
        if($value->beneficiary != "") {

            // validate
            Validator::make(['beneficiary' => $value->beneficiary, 'amount' => $value->amount, 'date' => $value->date, 'dk' => $value->dk], [
                'beneficiary' => 'required|string|max:191',
                'amount' => 'required|numeric',
                'date' => 'required|date',
                'dk' => 'required|string|max:1',
            ])->validate();

            $parsed_date = Carbon\Carbon::parse($value->date)->addDay()->toDateString();

            $benef = $beneficiaryService->handleBankBeneficiary($value->beneficiary);

            $transaction =  new Transaction([
                'date' => $parsed_date,
                'beneficiary' => $benef,
                'user_id' => auth('api')->user()->id,
                'totalAmount' => $value->amount,
                'processed' => 0,
                'statement_id' => $statement_id
            ]);

        $transaction->save();
        }
    }

    /** get and attach transaction categories suggestions */
    public function attachCategoriesSuggestions($transaction, $transactionsWithSameBen, $categories) 
    { 
        $beneficiaryService = new BeneficiaryService;
        $category = new ComponentCategory();

        $maxId = "";
        
        // if it's possible to find categories suggestions
        if (count($transactionsWithSameBen) > 0) {

            $beneficiaryResults = $this->getDistinctCategoriesAndMaxCount($transactionsWithSameBen);
            $maxId = $beneficiaryResults["maxId"];

             // get main suggestion category name 
            $category = ComponentCategory::ownUser()->find($maxId);

            $transaction->mainSuggestionCategory = $category->name;

             // mark suggested categories
            $transaction->categories = $this->getCategoriesSuggestions($categories, $beneficiaryResults["distinctCategories"]);

            $transaction->foundSuggestion = true;
        } else {
            $transaction->categories = [];
            $transaction->foundSuggestion = false;
        }

        $suggestedBeneficiary = $beneficiaryService->convertBeneficiaryName($transaction->customBeneficiary->name, true, 1);

        // if suggested beneficiary name changed, set new. Else set null
        $transaction->suggestedBeneficiaryName = $suggestedBeneficiary == $transaction->customBeneficiary->name ? null : $suggestedBeneficiary;

        return [$maxId, $transaction, $category];
    }

    public function getTransactionWithCategoriesSuggestion($transaction, $categories) 
    { 
        // all processed transactions with same beneficiary
        $transactionsWithSameBen = Transaction::ownUser()->processed(1)->where('beneficiary', $transaction->beneficiary)->get();

        [$maxId, $transaction, $category] = $this->attachCategoriesSuggestions($transaction, $transactionsWithSameBen, $categories);

        // creates one component for transaction on suggested category
        $transaction = $this->addSuggestedTransactionComponent($transaction, $maxId, $category->name);
        
        return $transaction;
    }

    /** adds one component for transaction on suggested category */
    public function addSuggestedTransactionComponent($transaction, $categoryId, $categoryName) 
    { 
        $transactionComponent = array("amount" => $transaction->totalAmount, "category_id" => $categoryId, "category_name" => $categoryName);
        $transactionComponents = array();

        array_push($transactionComponents, $transactionComponent);
        $transaction->transactionComponents = $transactionComponents;
        return $transaction;
    }

    public function getTransactionsCategoriesSuggestions($statement_id) 
    {
        $transactions = Transaction::ownUser()->with('CustomBeneficiary')->where('statement_id', $statement_id)->where('processed', 0)->get();

        $results = array();
        $categories = ComponentCategory::ownUser()->get();

        foreach ($transactions as $transaction) {
            $transaction = $this->getTransactionWithCategoriesSuggestion($transaction, $categories);
            array_push($results, $transaction);
        }

        $results = new Collection($results);
    
        return $results->sortBy("date")->values()->paginate(25);
    }

    /** mark every category which is equal to the one from categoriesDistinct as suggested */
    public function getCategoriesSuggestions($categories, $categoriesDistinct)
    {
        $category = array("id" => "", "name" => "", "suggested" => false);
        $results = array();
        foreach ($categories as $cat) {

            $category["id"] = $cat->id;
            $category["name"] = $cat->name;
            $category["suggested"] = false;

            foreach ($categoriesDistinct as $catDis) {
                if ($catDis == $cat->id) {
                    $category["suggested"] = true;
                }
            }

            array_push($results, $category);
        }

        return $results;
    }

    /** find category which repeated most times */
    public function findMaxCountCategory($allCategories)
    {
        $counted = array_count_values($allCategories);
        arsort($counted);
        return (key($counted));
    }

    /** get distinct categories and max repeats category id */
    public function getDistinctCategoriesAndMaxCount($transactions)
    {

        $allCategories = array();
        $categoriesDistinct = array();
        $maxId = "";

        foreach ($transactions as $transaction) {
            // all transaction component categories
            $categories = TransactionComponent::where('transaction_id', $transaction->id)->pluck('category_id');

            foreach ($categories as $category) {
                array_push($allCategories, $category);
            }
        }

        // find distinct catgories
        $categoriesDistinct = array_unique($allCategories);

         // if it's possible to find max count category
        if ($allCategories && $categoriesDistinct) {
            $maxId = $this->findMaxCountCategory($allCategories, $categoriesDistinct); // find max count category
        }

        return ["distinctCategories" => $categoriesDistinct, "maxId" => $maxId, "foundSuggestion" => $maxId != "" ? True : False];
    }

    /** if processed equals not delete processed or not processed transactions from statement
     * else delete statement and it's transactions
     */
    public function deleteStatement($id, $processed) {
        
        // statement transactions
        $transactions = Transaction::ownUser()->where("statement_id", $id);

        if ($processed != 2) { 
            $transactions = $transactions->where("processed", $processed);
        }

        $transactions_ids = $transactions->get()->pluck("id");

        TransactionComponent::whereIn("transaction_id", $transactions_ids)->delete();
        Transaction::ownUser()->whereIn("id", $transactions_ids)->delete();

        if($processed == 2) { 
            Statement::ownUser()->find($id)->delete();
        }

        return ['message' => 'Transactions Deleted'];
    }

    public function importStatement($statement) { 
        $file_name = "new" . auth('api')->user()->id . ".csv";
        $file_path = 'storage/app/storage/uploads/' . $file_name;

        Storage::putFileAs('/storage/uploads', $statement, $file_name);

        $data = Excel::load($file_path, function ($reader) { })->get();
        if (!empty($data) && $data->count()) {

            // create statement
            $statement = Statement::create([
            "name" => $statement->getClientOriginalName(), 
            "user_id" => auth('api')->user()->id]);

            // create transactions for each file row
            foreach ($data as $value) {
                $this->createTransactionFromFile($value, $statement->id);
            }
        }

        // delete file from storage
        Storage::delete("storage\uploads\\" . $file_name);
        return ['message' => 'Imported successfully'];
    }

}
