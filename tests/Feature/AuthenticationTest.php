<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Laravel\Passport\Passport;
use App\User;
use App\ComponentCategory;
use App\Services\TestService;

class AuthenticationTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;

    protected function setUp(): void
    {
        $this->createApplication();

        parent::setUp();
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }

    public function testRegister() { 

     $data = [
         "name" => "tadas",
         "email" => "tadas15@gmail.com",
         "password" => "banban",
         "password_confirmation" => "banban"
     ];

      $this->post('/register', $data)->assertStatus(302);

      $user = User::where("email", $data["email"])->first();

      $this->assertNotNull($user);

      $this->assertEquals($user->email, $data["email"]);
      $this->assertEquals($user->type, "client");
    }

    public function testLogin() {
        $user = factory(User::class)->create();
        $user->password = "test123";
        $user->save();

        $this->post('/login', [
            'email' => $user->email,
            'password' => $user->password,
        ])->assertStatus(302);
    }
}