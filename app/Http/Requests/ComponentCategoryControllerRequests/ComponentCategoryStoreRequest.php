<?php

namespace App\Http\Requests\ComponentCategoryControllerRequests;
use App\Rules\UniqCategory;
use Illuminate\Foundation\Http\FormRequest;

class ComponentCategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'min:2','max:191', new UniqCategory],
        ];
    }
}
