<?php


namespace App\Services;

use App\Services\StatsDataService;
use App\ComponentCategory;
use App\Transaction;
use Carbon;
use Illuminate\Support\Collection;

class StatsCalcService
{
    public function getMonthsStatsData($dateFrom, $dateTo, $categoryId, $beneficiaryId, $paginate, $needConvertDate)
    {
        $statsDataService = new StatsDataService;
        $statsCalcService = new StatsCalcService;

        if($needConvertDate) {
        [$dateFrom, $dateTo] = $statsDataService->formatNullDates($dateFrom, $dateTo);
        }

        $dates = array();
        $results = array();
        $foundData = false;
        
        $transactions = $statsDataService->categoryOrBeneficiaryTransactions($categoryId, $beneficiaryId, $dateFrom, $dateTo, null)->get();

        if ($transactions->count() > 0) {
            $dates = $statsCalcService->getDatesFromInterval($transactions->min('date'), $transactions->max('date'));
            $results = $statsCalcService->getDatesAmounts($dates, $transactions);
            $foundData = true;
        }

        if ($paginate) {
            $collection = (new Collection($results));
            return ["results" => $collection->sortByDesc('date')->values()->paginate(8), "foundData" => $foundData];
        } else {
            return ["results" => $results, "foundData" => $foundData];
        }
    }

    /** Return all months from date interval in Y-m format */
    public function getDatesFromInterval($dateFrom, $dateTo)
    {
        $dates = array();

        // start date
        $date = date("Y-m", strtotime($dateFrom));

        while (strtotime($date) <= strtotime($dateTo)) {
            array_push($dates, $date);
            $date = date("Y-m", strtotime("+1 month", strtotime($date)));
        }

        return $dates;
    }

    /** Get amounts sum for every month */
    public function getDatesAmounts($dates, $transactions)
    {
        $dateResults = array(
            "date" => "",
            "sum" => ""
        );

        $results = array();

        foreach ($dates as $date) {

            $sum = 0;
            foreach ($transactions as $transaction) {
                // parse year
                $year = date("Y", strtotime($transaction->date));
                // parse month
                $month = date("m", strtotime($transaction->date));
                $month = ltrim($month, '0');
                if ($year == ltrim(date("Y", strtotime($date)), 0) && $month == ltrim(date("m", strtotime($date)), 0)) {
                     // format number
                    $sum = $this->add($sum, $transaction->amount);
                }
            }

            $dateResults["date"] = $date;
            $dateResults["sum"] = round($sum, 2);
            array_push($results, $dateResults);
        }

        return $results;
    }

    /**  Get categories beneficiary sum */
    public function getCategoryBeneficiariesStatsData($dateFrom, $dateTo, $needPercentage, $categoryId, $paginate, $needConvertDate)
    {
        $statsDataService = new StatsDataService;

        if ($needConvertDate) {
           [$dateFrom, $dateTo] = $statsDataService->formatNullDates($dateFrom, $dateTo);
        }

        $results = array();

        $results = $statsDataService->categoryBeneficiariesWithAmountsSums($categoryId, null, $dateFrom, $dateTo);

        // if needed, compute amounts percentage
        if ($needPercentage) {
            $results = $this->computeAmountsPercentage($results);
        }

        $foundData = count($results) > 0 ? true : false;

        if ($paginate) {
            $collection = (new Collection($results));
            return ["results" => $collection->sortByDesc('amount')->values()->paginate(5), "foundData" => $foundData];
        } else {
            return ["results" => $results, "foundData" => $foundData];
        }
    }

    /** compute category amount */
    public function computeCategoryAmount($category, $dateFrom, $dateTo) { 

        $statsDataService = new StatsDataService;

        $categoryResults = array(
            "name" => "",
            "amount" => "",
            "id" => ""
        );

        $amount_sum = round($statsDataService->categoryOrBeneficiaryTransactions($category->id, null, $dateFrom, $dateTo, null)->sum("amount"), 2);

        $categoryResults["id"] = $category->id;
        $categoryResults["name"] = $category->name;
        $categoryResults["amount"] = $amount_sum;
        
        return $categoryResults;
    }

    public function getCategoriesStatsData($dateFrom, $dateTo, $needPercentage, $paginate, $needConvertDate)
    {
        $statsDataService = new StatsDataService;

        if ($needConvertDate) {
            [$dateFrom, $dateTo] = $statsDataService->formatNullDates($dateFrom, $dateTo);
        } 

        $results = array();
        $foundData = false;

        $categories = ComponentCategory::ownUser()->orderBy("position")->get();

        foreach ($categories as $category) {

            $category = $this->computeCategoryAmount($category, $dateFrom, $dateTo);

            if ($category["amount"]) {
                $foundData = true;
            }

            array_push($results, $category);
        }

        // if needed, compute amounts percentage
        if ($needPercentage) {
            $results = $this->computeAmountsPercentage($results);
        }

        if ($paginate == 1) {
            $collection = (new Collection($results));
            return ["results" => $collection->sortByDesc('amount')->values()->paginate(5), "foundData" => $foundData];
        } else {
            return ["results" => $results, "foundData" => $foundData];
        }
    }

    public function computeSavingCampaignsStats($dateFrom, $dateTo, $categories, $statsType, $categoryId)
    {

        $statsDataService = new StatsDataService;

        $categoryDatesStats = array();

        $stats = $statsDataService->prepareResultsDataSetForSavCamp($categories, $dateFrom, $dateTo);

        $dates = $statsDataService->prepareDates($dateFrom, $dateTo);

        foreach ($stats as &$statsCategory) {

            foreach ($dates as &$date) {

                // get wanted amounts sum
                $wantedAmountsSum = $this->getSavingCampaignCategoryWantedAmount($statsCategory["id"], $date);

                // get spent amounts sum 
                $amountsSum = $statsDataService->categoryOrBeneficiaryTransactions($statsCategory["id"], null, null, null, $date)->sum("amount");

                // add wanted amounts sum and substract it by spent amounts sum for each category
                if ($statsType != "months") {
                    $statsCategory["amount"] = round($statsCategory["amount"] + $wantedAmountsSum, 2);
                    $statsCategory["amount"] = round($statsCategory["amount"] - $amountsSum, 2);
                }

                if ($statsType != "categories") {
                    $date["amount"] = round($date["amount"] + $wantedAmountsSum, 2);
                    $date["amount"] = round($date["amount"] - $amountsSum, 2);
                }

                // if needed to get results only for one category
                if ($categoryId && $categoryId == $statsCategory["id"]) {
                    $categoryDateStats = array("amount" => round($wantedAmountsSum - $amountsSum, 2), "date" => $date["year"] . "-" . $date["month"]);
                    array_push($categoryDatesStats, $categoryDateStats);
                }
            }
        }

        return ["categoriesStats" => $stats, "totalStats" => $dates, "categoryStats" => $categoryDatesStats];
    }

    public function getSavingCampaignCategoryWantedAmount($categoryId, $date)
    {
        $wantedAmount = 0;

        $callback = function ($query) use ($date) {
            $query->whereYear("date", $date["year"])->whereMonth("date", $date["month"]);
        };

        // get categories with related to them saving campaigns
        $category = ComponentCategory::ownUser()->where("id", $categoryId)->with(['savingCampaign' => $callback])->first();

        if ($category) {

            // sum wanted amounts for all categories on saving campaign
            foreach ($category->savingCampaign as $camp) {
                $wantedAmount += $camp->pivot->wanted_amount;
            }
        }

        return $wantedAmount;
    }

    public function getSavingCampaignsChartsStatsData($dateFrom, $dateTo, $paginate, $categoryId, $statsType)
    {
        [$dateFrom, $dateTo] = (new StatsDataService)->getDatesForSavingCampChartsStats($dateFrom, $dateTo);
        
        if ($categoryId && $statsType != "all") {
            $categories = ComponentCategory::ownUser()->where("id", $categoryId)->get();
        } else {
            $categories = ComponentCategory::ownUser()->get();
        }

        $results = $this->computeSavingCampaignsStats($dateFrom, $dateTo, $categories, $statsType, $categoryId);

        if ($paginate != null) {

           return $this->paginateSavingCampaignsChartsData($paginate, $results);
        } else {
            return ["categoriesStats" => $results["categoriesStats"], "totalStats" => $results["totalStats"], "categoryStats" => $results["categoryStats"]];
        }
    }

    public function paginateSavingCampaignsChartsData($paginate, $results) { 

        if ($paginate == "total") {
            $collection = (new Collection($results["totalStats"]));
        } else if ($paginate == "categories") {
            $collection = (new Collection($results["categoriesStats"]));
        } else {
            $collection = (new Collection($results["categoryStats"]));
        }

        return ["results" => $collection->sortByDesc('amount')->values()->paginate(5)];
    }

    /** compute every category amounts percentage */
    public function computeAmountsPercentage($results) // todo change for to foreach
    {
        $sum = 0;
        foreach ($results as $result) {
            $sum += (float) $result["amount"];
        }

        for ($i = 0; $i < count($results); $i++) {

            $temp = $sum == 0 ? 0 : 100 / $sum;

            if ($temp == 0) {
                $results[$i]["percentage"] = 0;
            } else {
                $results[$i]["percentage"] = round($temp * (float) $results[$i]["amount"], 2);
            }
        }

        return $results;
    }

    public function computeExpensesAnnualMonthlyAverage($sum, $daysDiff)
    {
        if (($daysDiff == 0) || ($daysDiff < 30 && $sum > 0)) {
            return $sum;
        } else if ($sum > 0) {

            return round(($sum * 30) / $daysDiff, 2);
        } else {
            return 0;
        }
    }

    public function computeDifferenceInPercentage($firstValue, $secondValue)
    {
        if ($secondValue == 0 && $firstValue > 0) {
            return 100;
        } else if ($secondValue > 0 && $firstValue == 0) {
            return -100;
        } else if ($secondValue == 0) {
            return 0;
        }
         else {
            return round((($firstValue - $secondValue) / $firstValue) * 100, 2);
        }
    }

    public function checkIfChartsAreEmpty($categoriesStats) {

        foreach($categoriesStats as $category) { 
            if($category["amount"] != 0) { 
                return false;
            }
        }

        return true;
    }

    /** get numbers difference in three formats: difference in percentage, formatted difference in percentage and usual */
    public function getNumbersDifference($firstValue, $secondValue) { 

        $statsCalcService = new StatsCalcService;

        $diff = round(($firstValue - $secondValue), 2);

        $diffInPerc = $statsCalcService->computeDifferenceInPercentage($firstValue, $secondValue);

        $diffInPercFormatted = $diffInPerc;

        if ($diffInPerc >= 100) {
            $diffInPercFormatted = ">=" . strval(100);
        } else if ($diffInPerc <= -100) {
            $diffInPercFormatted = "<=" . strval(-100);
        }

        return [$diffInPerc, $diff, $diffInPercFormatted];
    }

    /** add two numbers */
    public function add($number1, $number2) {
        $number1 = number_format((float)$number1, 2, '.', '');
        $number2 = number_format((float)$number2, 2, '.', ''); 
        return number_format((float)$number1 + $number2, 2, '.', ''); 
    }

    /** compute saving status. Options: 3 2 1 0 -1 -2 -3 */
    public function getSavingsStatus($perc, $isSavingCampStat)
    {
        if ($perc < 0) {
            if ($perc <= -100) {
                return -3;
            } else if ($perc >= -10) {
                return -1;
            } else {
                return round($perc / 10 / 3);
            }
        } else if ($perc > 0 && $perc <= 10) {
             // if it's saving campaign stats set different status
            if($isSavingCampStat) {
            return 0;
            }
            else { 
                return 1;
            }
        } else if ($perc >= 100) {
            return 3;
        } else {
            return round($perc / 10 / 3);
        }
    }
}
