<?php

namespace App\Http\Requests\SavingCampaignControllerRequests;

use App\Rules\UniqDateSavingCampaign;
use Illuminate\Foundation\Http\FormRequest;
use Route;

class SavingCampaignUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'income' => 'required|numeric',
            'name' => 'required|string|max:191',
            'date' =>  ['required','date', new UniqDateSavingCampaign(Route::input('saving_campaign'))],
            'categories.*.wanted_amount' => 'required|numeric',
            'categories.*.id' => 'required|numeric'
        ];
    }
}
