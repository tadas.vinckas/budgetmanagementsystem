<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable2 extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->tinyInteger('processed')->default('0');
            $table->increments('id');
            $table->integer('beneficiary')->unsigned()->index()->nullable();
            $table->foreign('beneficiary')->references('id')->on('custom_beneficiaries');
            $table->decimal('totalAmount')->nullable();
            $table->date('date')->nullable();
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
