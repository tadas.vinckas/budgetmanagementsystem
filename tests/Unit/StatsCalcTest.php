<?php 

namespace Tests\Unit;
use Tests\TestCase;
use App\Services\StatsCalcService;
use App\Services\TestService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\ComponentCategory;
use App\CustomBeneficiary;

class StatsCalcTest extends TestCase 
{
    use RefreshDatabase;

    public function testGetDatesFromInterval()
    {
        $dateFrom = "2020-10-05";
        $dateTo = "2021-03-05";
        $dates = (new StatsCalcService)->getDatesFromInterval($dateFrom, $dateTo);

        $results = ["2020-10","2020-11","2020-12","2021-01","2021-02","2021-03"];
        $this->assertEquals($dates, $results);
    }

    public function testGetDatesAmounts() { 

        $user = factory(User::class)->create();

        [$transaction1,] = (new TestService)->createTransaction(null,null,"2020-08-03",$user);
        [$transaction2,] = (new TestService)->createTransaction(null,null,"2020-08-13",$user);
        [$transaction3,] = (new TestService)->createTransaction(null,null,"2020-09-13",$user);
        [$transaction4,] = (new TestService)->createTransaction(null,null,"2020-09-23",$user);

        $transaction1->amount = 12;
        $transaction2->amount = 13;
        $transaction3->amount = 14;
        $transaction4->amount = 15;

        $transactions = [];
        $dates = ["2020-08", "2020-09", "2020-10"];

        array_push($transactions, $transaction1, $transaction2, $transaction3, $transaction4);

        $results = (new StatsCalcService)->getDatesAmounts($dates, $transactions);

        $assertResults = [   
            array(
                "date" => "2020-08",
                "sum" => round($transaction1->amount + $transaction2->amount)
            )
            ,
            array(
                "date" => "2020-09",
                "sum" =>  round($transaction3->amount + $transaction4->amount)
            ),
            array(
                "date" => "2020-10",
                "sum" => 0.0
            )
            ];
           
       $this->assertEquals($results, $assertResults);
    }

    public function testComputeAmountsPercentage() { 
        $amounts = [ 
            array(
            "amount" => 20, "percentage" => 0
            ),
            array(
                "amount" => 40, "percentage" => 0
            ),
            array(
                "amount" => 40, "percentage" => 0
            ),
        ];

        $results = (new StatsCalcService)->computeAmountsPercentage($amounts);

        
        $assertResults = [   
            array(
                    "amount" => 20, "percentage" => 20.0
                ),
                array(
                    "amount" => 40, "percentage" => 40.0
                ),
                array(
                    "amount" => 40, "percentage" => 40.0
                ),
            ];

        $this->assertEquals($results, $assertResults);
    }

    /**
     * @dataProvider monthlyAverageDataProvider
     */
    public function testComputeExpensesAnnualMonthlyAverage($data) { 
        $results = (new StatsCalcService)->computeExpensesAnnualMonthlyAverage($data["sum"], $data["daysDiff"]);
        $this->assertEquals($data["result"], $results);

    }
    

    public function monthlyAverageDataProvider()
    {
        return [
            'days diff less than 30' => [[
                'daysDiff' => 20,
                'sum' => 30,
                'result' => 30
            ]],
            'days diff more than 30' => [[
                'daysDiff' => 40,
                'sum' => 30,
                'result' => round(((30*30)/40),2)
            ]],
            'days diff 0' => [[
                'daysDiff' => 10,
                'sum' => 30,
                'result' => 30
            ]],
            'days diff less than 30, sum 0' => [[
                'daysDiff' => 20,
                'sum' => 0,
                'result' => 0
            ]],
            'days diff more than 30, sum 0' => [[
                'daysDiff' => 40,
                'sum' => 0,
                'result' => 0
            ]],
        ];
    }

     /**
     * @dataProvider numbersDifferenceDataProvider
     */
    public function testComputeDifferenceInPercentage($data)
    {
        $result = (new StatsCalcService)->computeDifferenceInPercentage($data["first"], $data["second"]);
        $this->assertEquals($data["result"], $result);
    }

    public function numbersDifferenceDataProvider()
    {
        return [
            'first equals 0' => [[
                'first' => 0,
                'second' => 30,
                'result' => -100
            ]],
            'second equals 0' => [[
                'first' => 30,
                'second' => 0,
                'result' => 100
            ]],
            'normal' => [[
                'first' => 20,
                'second' => 30,
                'result' => round((20-30)/20*100,2)
            ]]
        ];
    }

    public function testAdd() { 
        $first = "2.02";
        $second = "4.02";
        $result = (new StatsCalcService)->add($first, $second);
        $this->assertEquals($result, "6.04");
    }

    /**
     * @dataProvider savingStatusDataProvider
     */
    public function testGetSavingStatus($data) { 
        $result = (new StatsCalcService)->getSavingsStatus($data["perc"], $data["result"]);
        $this->assertEquals($data["result"], $result);
    }

    public function savingStatusDataProvider()
    {
        return [
            'status 0' => [[
                'perc' => 0,
                'result' => 0,
            ]],
            'status 1' => [[
                'perc' => 20,
                'result' => 1,
            ]],
            'status 2' => [[
                'perc' => 50,
                'result' => 2,
            ]],
            'status 3' => [[
                'perc' => 80,
                'result' => 3,
            ]],
            'status 3 (2)' => [[
                'perc' => 160,
                'result' => 3,
            ]],
            'status -1' => [[
                'perc' => -5,
                'result' => -1,
            ]],
            'status -1 (2)' => [[
                'perc' => -10,
                'result' => -1,
            ]],
            'status -2' => [[
                'perc' => -50,
                'result' => -2,
            ]],
            'status -3' => [[
                'perc' => -80,
                'result' => -3,
            ]],
            'status -3 (2)' => [[
                'perc' => -120,
                'result' => -3,
            ]],
            'status -3 (33)' => [[
                'perc' => -1200,
                'result' => -3,
            ]],
        ];
    }
}