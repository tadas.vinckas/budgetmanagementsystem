<?php

namespace App\Services;

use App\CustomBeneficiary;
use App\BankBeneficiary;
use App\ComponentCategory;
use App\User;
use App\Transaction;
use App\TransactionComponent;

class TestService
{
    public function createCustomBeneficiaryWithBankAssociation($relatedToBank, $user)
    {
        $customBeneficiary = factory(CustomBeneficiary::class)->create([
            "user_id" => $user->id,
            "name" => "rimi",
            "relatedToBank" => $relatedToBank
        ]);

        $bankBeneficiary = factory(BankBeneficiary::class)->create();

        $customBeneficiary->bankBeneficiaries()->attach($bankBeneficiary->id);

        return $customBeneficiary;
    }

    public function formatNumber($number) {
        $numberParts = explode(".", $number);
        if(strlen($numberParts[1]) == 1) {
           return strval($number) . "0";
        }
        return strval($number);
    }

    public function createTransaction($categoryId, $customBeneficiaryId, $date, $user)
    {

        $customBeneficiary = null;

        if (!$categoryId) {  // create category
            $category = factory(ComponentCategory::class)->create([
                "user_id" => $user->id,
            ]);

            $categoryId = $category->id;
        }

        if (!$customBeneficiaryId) {

            $customBeneficiary = factory(CustomBeneficiary::class)->create([
                "user_id" => $user->id
            ]);

            $customBeneficiaryId = $customBeneficiary->id;
        }

        $transaction = factory(Transaction::class)->create([
            "user_id" => $user->id,
            "beneficiary" => $customBeneficiaryId,
            "date" => $date ? $date : "2020-08-01"
        ]);

        $transactionComponent = factory(TransactionComponent::class)->create([
            "transaction_id" => $transaction->id,
            "category_id" => $categoryId
        ]);

        $transaction->totalAmount = $transactionComponent->amount;
        $transaction->save();
        return [$transaction, $customBeneficiary];
    }

    public function computePercentage($sum, $amount) { 
        return (double)$this->formatNumber(round((100/$sum)*$amount,2));
    }

    public function prepareData($user) { 
        $categories = factory(ComponentCategory::class, 2)->create([
            "user_id" => $user->id,
         ]);
   
         $customBeneficiary = factory(CustomBeneficiary::class)->create([
            "user_id" => $user->id
         ]);

         $transactions = array();
   
        [$transaction,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary, "2020-07-01", $user);
        [$transaction2,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary, "2020-07-01", $user);
        [$transaction3,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary, "2020-08-01", $user);
        [$transaction4,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary, "2020-09-01", $user);
        [$transaction5,] = (new TestService)->createTransaction($categories[1]->id, $customBeneficiary, "2020-08-01", $user);
        [$transaction6,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-09-01", $user);
        [$transaction7,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiary, "2020-11-01", $user);
    
         array_push($transactions, $transaction, $transaction2, $transaction3, $transaction4, $transaction5, $transaction6, $transaction7);
         
         return [$transactions, $customBeneficiary, $categories];
    }
}