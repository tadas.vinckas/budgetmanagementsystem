<?php

namespace Tests\Feature\StatsTests;

use Tests\TestCase;
use App\Services\TestService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\User;
use Laravel\Passport\Passport;
use App\ComponentCategory;
use App\CustomBeneficiary;

class CategoryBeneficiariesStatsTest extends TestCase
{
   use WithoutMiddleware, RefreshDatabase;

   protected function setUp(): void
   {
      $this->createApplication();

      parent::setUp();
   }

   protected function tearDown(): void
   {
      parent::tearDown();
   }
   // todo assert json

   public function testCategoryBeneficiariesStatsOnDifferentCategoriesAndBeneficiaries()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiaries = factory(CustomBeneficiary::class, 2)->create([
         "user_id" => $user->id
      ]);

      $categories = factory(ComponentCategory::class, 2)->create([
         "user_id" => $user->id,
      ]);


      $customBeneficiaries[0]->name = "abc";
      $customBeneficiaries[1]->name = "zyx";

      $customBeneficiaries[0]->save();
      $customBeneficiaries[1]->save();

      [$transaction,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiaries[0]->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($categories[0]->id, $customBeneficiaries[1]->id, "2020-07-01", $user);
      [$transaction3,] = (new TestService)->createTransaction($categories[1]->id, null, "2020-09-01", $user);

      $sum = $transaction->totalAmount + $transaction2->totalAmount;

      $this->get('api/category-beneficiaries-stats/' . $categories[0]->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . "?paginate=1")->assertStatus(200)
         ->assertJsonFragment([
            [
               "percentage" => (new TestService)->computePercentage($sum, $transaction->totalAmount),
               "amount" => (new TestService)->formatNumber($transaction->totalAmount),
               "beneficiary" => $customBeneficiaries[0]->id,
               "beneficiary_name" => $customBeneficiaries[0]->name
            ]
         ])
         ->assertJsonFragment([
            [
               "amount" => (new TestService)->formatNumber($transaction2->totalAmount),
               "beneficiary" => $customBeneficiaries[1]->id,
               "beneficiary_name" => $customBeneficiaries[1]->name,
               "percentage" => (new TestService)->computePercentage($sum, $transaction2->totalAmount)
            ]
         ]);
   }


   public function testCategoryBeneficiariesStatsBeneficiariesSum()
   {
      $dates = array("dateFrom" => "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user);

      $response = $this->get('api/category-beneficiaries-stats/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . "?paginate=1")->assertStatus(200)
         ->assertJsonFragment([
            "amount" => (new TestService)->formatNumber(round($transaction->totalAmount + $transaction2->totalAmount, 2)),
            "beneficiary" => $customBeneficiary->id,
            "beneficiary_name" =>  $customBeneficiary->name,
            "percentage" => 100
         ]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
      $this->assertEquals(count($response), 1);
   }

   public function testCategoryBeneficiariesStatsDateIntervalFilterFoundData()
   {
      $dates = array("dateFrom" =>  "2020-05-22", "dateTo" => "2020-08-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user);
      [$transaction2,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-09-01", $user);

      $response = $this->get('api/category-beneficiaries-stats/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
         ->assertJsonFragment([
            "amount" => strval(number_format($transaction->totalAmount, 2, '.', ' ')),
            "beneficiary" => $customBeneficiary->id,
            "beneficiary_name" =>  $customBeneficiary->name,
            "percentage" => 100
         ]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
      $this->assertEquals(count($response), 1);
   }

   public function testCategoryBeneficiariesStatsDateIntervalFilterNotFoundData()
   {
      $dates = array("dateFrom" =>  "2021-05-22", "dateTo" => "2021-08-22");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user);

      $response = $this->get('api/category-beneficiaries-stats/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);
      $this->assertEquals(count($response), 0);
   }

   public function testCategoryBeneficiariesStatsAllTimeFoundData()
   {
      $dates = array("dateFrom" =>  "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $customBeneficiary = factory(CustomBeneficiary::class)->create([
         "user_id" => $user->id
      ]);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      [$transaction,] = (new TestService)->createTransaction($category->id, $customBeneficiary->id, "2020-07-01", $user);

      $response = $this->get('api/category-beneficiaries-stats/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . '?paginate=1')->assertStatus(200)
         ->assertJsonFragment([
            "amount" => (new TestService)->formatNumber($transaction->totalAmount),
            "beneficiary" => $customBeneficiary->id,
            "beneficiary_name" =>  $customBeneficiary->name,
            "percentage" => 100
         ]);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, true);
      $this->assertEquals(count($response), 1);
   }

   public function testCategoryBeneficiariesStatsAllTimeNotFoundData()
   {
      $dates = array("dateFrom" =>  "null", "dateTo" => "null");

      $user = factory(User::class)->create();
      Passport::actingAs($user);

      $category = factory(ComponentCategory::class)->create([
         "user_id" => $user->id,
      ]);

      $response = $this->get('api/category-beneficiaries-stats/' . $category->id . '/' . $dates["dateFrom"] . '/' . $dates["dateTo"] . "?paginate=1")->assertStatus(200);

      $responseContent = json_decode($response->getContent());
      $response = $responseContent->results->data;

      $this->assertEquals($responseContent->foundData, false);

      $this->assertEquals(count($response), 0);
   }
}
