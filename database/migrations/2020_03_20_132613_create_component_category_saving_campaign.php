<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponentCategorySavingCampaign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('component_category_saving_campaign', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('component_category_id')->unsigned()->index()->nullable();
            $table->foreign('component_category_id')->references('id')->on('component_categories');
            $table->integer('saving_campaign_id')->unsigned()->index()->nullable();
            $table->foreign('saving_campaign_id')->references('id')->on('saving_campaigns');   
            $table->decimal('wantedAmount')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('component_category_saving_campaign');
    }
}
