<?php

use Illuminate\Database\Seeder;
use App\User;

class TransactionCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();

        DB::table('component_categories')->insert([
            'name' => 'keliones',
            'user_id' => $user->id
        ]);

        DB::table('component_categories')->insert([
            'name' => 'maistas',
            'user_id' => $user->id
        ]);

        DB::table('component_categories')->insert([
            'name' => 'mokesciai',
            'user_id' => $user->id
        ]);

        DB::table('component_categories')->insert([
            'name' => 'pramogos',
            'user_id' => $user->id
        ]);
    }
}
