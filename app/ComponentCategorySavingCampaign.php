<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComponentCategorySavingCampaign extends Model
{
    
    protected $fillable = ['category_id', 'savingCampaign_id', 'wanted_amount'];

}
