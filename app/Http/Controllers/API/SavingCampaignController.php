<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\SavingCampaignControllerRequests\SavingCampaignStoreRequest;
use App\Http\Requests\SavingCampaignControllerRequests\SavingCampaignUpdateRequest;
use Illuminate\Http\Request;
use App\SavingCampaign;
use App\Services\SavingCampaignService;
use Carbon;

class SavingCampaignController extends Controller
{

       /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
        $this->campaignService = new SavingCampaignService;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $campaigns = SavingCampaign::ownUser()->get();
       
        // todo use observer
        foreach ($campaigns as $camp) { // updates status 
            $camp->active = $this->campaignService->getActiveStatus($camp->date);
            $camp->save();
        }

        return SavingCampaign::ownUser()->orderBy('date', 'desc')->with('componentCategories')->paginate(5);
    }


    public function statistics($id)
    {
        if ($id == "null") { // return campaign, priority for active
            $camp = SavingCampaign::ownUser()->where('active', '!=', 2)->orderBy('active', 'desc')->first();
        } else {
            $camp = SavingCampaign::ownUser()->findOrFail($id);
        }

        return $this->campaignService->getCampaignStats($camp);
    }

    public function categories()
    {
        return $this->campaignService->getCategoriesData();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SavingCampaignStoreRequest $request)
    {
        return $this->campaignService->storeSavingCampaign($request->name, $request->date, $request->income, $request->categories);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return SavingCampaign::ownUser()->where("id", $id)->with('componentCategories')->first();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SavingCampaignUpdateRequest $request, $id)
    {
        return $this->campaignService->updateSavingCampaign($id, $request->name, $request->date, $request->income, $request->categories);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $campaign = SavingCampaign::ownUser()->findOrFail($id);

        $campaign->componentCategories()->detach();

        $campaign->delete();

        return ['message' => "Deleted successfuly"];
    }
}
