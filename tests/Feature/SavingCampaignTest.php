<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use App\User;
use App\Transaction;
use App\ComponentCategory;
use App\TransactionComponent;
use App\CustomBeneficiary;
use App\BankBeneficiary;
use App\SavingCampaign;
use App\Services\TestService;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Carbon;

class SavingCampaignTest extends TestCase
{
    use WithoutMiddleware, RefreshDatabase;
    protected function setUp(): void
    {
        $this->createApplication();
        parent::setUp();
    }

    protected function tearDown(): void
    {

        parent::tearDown();
    }


    public function datesDataProvider()
    {
        return [
            '0' => [[
                'date' => Carbon\Carbon::now()->subYear()->toDateString(),
                'status' => 0
            ]],
            '1' => [[
                'date' => Carbon\Carbon::now()->toDateString(),
                'status' => 1
            ]],
            '2' => [[
                'date' => Carbon\Carbon::now()->addYear()->toDateString(),
                'status' => 2
            ]],
        ];
    }

    /**
     * @dataProvider datesDataProvider
     */
    public function testSavingCampaignIndexAndActiveStatus($data)
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $campaign = factory(SavingCampaign::class)->create([
            "user_id" => $user->id,
            "date" => $data["date"]
        ]);

        $this->get('api/saving-campaign?paginate=1')->assertStatus(200)
        ->assertJsonFragment([
            "income" => (new TestService)->formatNumber($campaign->income),
            "name" => $campaign->name,
            "date" => $campaign->date,
            "active" => $data["status"]
        ]);
    }

    public function testSavingCampaignCreate()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $categories = factory(ComponentCategory::class,2)->create([
            "user_id" => $user->id
        ]);

        $campaignToCreate = [
            'income' => 200.20,
            'name' => "august savings",
            'date' => "2020-08-19",
            'categories' => [
                [
                "id" => $categories[0]->id,
                 "wanted_amount" => "5.00"
                ],
                [
                "id" => $categories[1]->id,
                "wanted_amount" => "5.00"
                ]
            ]
        ];
        $this->post('api/saving-campaign', $campaignToCreate)->assertStatus(200);

        $campaign = SavingCampaign::first();

        $this->assertEquals($campaign->income, $campaignToCreate["income"]);
        $this->assertEquals($campaign->name, $campaignToCreate["name"]);
        $this->assertEquals($campaign->date, $campaignToCreate["date"]);
        $this->assertEquals($campaign->componentCategories[0]->pivot->wanted_amount, $campaignToCreate["categories"][0]["wanted_amount"]);
        $this->assertEquals($campaign->componentCategories[1]->pivot->wanted_amount, $campaignToCreate["categories"][1]["wanted_amount"]);
    }

    public function testSavingCampaignUpdate()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $campaignToUpdate = factory(SavingCampaign::class)->create([
            "user_id" => $user->id
        ]);

        $categories = factory(ComponentCategory::class,2)->create([
            "user_id" => $user->id
        ]);

        $campaignToUpdate->componentCategories()->attach($categories[0]->id, ["wanted_amount" => "2.17"]);
        $campaignToUpdate->componentCategories()->attach($categories[1]->id, ["wanted_amount" => "5.17"]);
        $campaignToUpdate->save();

        $campaignUpdateData = [
         //   'id' => $campaignToUpdate->id,
            'income' => 200.20,
            'name' => "august savings",
            'date' => "2020-08-19",
            'categories' => [
                [
                "id" => $categories[0]->id,
                 "wanted_amount" => "7.00"
                ],
                [
                "id" => $categories[1]->id,
                "wanted_amount" => "8.00"
                ]
            ]
        ];

        $this->put('api/saving-campaign/' . $campaignToUpdate->id, $campaignUpdateData)->assertStatus(200);

        $campaign = SavingCampaign::find($campaignToUpdate->id);

        $this->assertEquals($campaign->income, $campaignUpdateData["income"]);
        $this->assertEquals($campaign->name, $campaignUpdateData["name"]);
        $this->assertEquals($campaign->date, $campaignUpdateData["date"]);
        $this->assertEquals($campaign->componentCategories[0]->pivot->wanted_amount, $campaignUpdateData["categories"][0]["wanted_amount"]);
        $this->assertEquals($campaign->componentCategories[1]->pivot->wanted_amount, $campaignUpdateData["categories"][1]["wanted_amount"]);
    }


    public function testSavingCampaignDelete()
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $campaign = factory(SavingCampaign::class)->create([
            "user_id" => $user->id
        ]);
     
        $this->delete(route('saving-campaign.destroy', $campaign->id))->assertStatus(200);

        $this->assertNull(SavingCampaign::find($campaign->id));
    }
    
    public function testSavingCampaignStats() 
    {
        $user = factory(User::class)->create();
        Passport::actingAs($user);

        $campaign = factory(SavingCampaign::class)->create([
            "user_id" => $user->id
        ]);

        $campaign->date = "2020-09-01";
        $campaign->save();

        $categories = factory(ComponentCategory::class,2)->create([
            "user_id" => $user->id
        ]);

        $categories[0]->name = "aa";
        $categories[1]->name = "bb";
        $categories[0]->save();
        $categories[1]->save();
+
        $campaign->componentCategories()->attach($categories[0]->id, ["wanted_amount" => "2.17"]);
        $campaign->componentCategories()->attach($categories[1]->id, ["wanted_amount" => "5.17"]);

        [$transaction,] = (new TestService)->createTransaction($categories[0]->id, null, $campaign->date, $user);
        [$transaction2,] = (new TestService)->createTransaction($categories[1]->id, null, $campaign->date, $user);
        [$transaction3,] = (new TestService)->createTransaction($categories[1]->id, null, $campaign->date, $user);
        [$transaction4,] = (new TestService)->createTransaction($categories[0]->id, null, "2020-08-01", $user);
      
        $response = $this->get("/api/saving-campaign/statistics/" . $campaign->id)->assertStatus(200);

        $responseContent = json_decode($response->getContent());
        $response = $responseContent->campaignStats;
    
        $this->assertEquals($response[0]->pivot->wanted_amount, $campaign->componentCategories[0]->pivot->wanted_amount);
        $this->assertEquals($response[1]->pivot->wanted_amount, $campaign->componentCategories[1]->pivot->wanted_amount);

        $this->assertEquals($response[0]->amountSpent, $transaction->totalAmount);
        $this->assertEquals($response[1]->amountSpent, $transaction2->totalAmount + $transaction3->totalAmount);

        $spendsSum = round($transaction->totalAmount + $transaction2->totalAmount + $transaction3->totalAmount, 2);
        $wantedAmountsSum = round($campaign->componentCategories[0]->pivot->wanted_amount + $campaign->componentCategories[1]->pivot->wanted_amount,2);

        $this->assertEquals($responseContent->spendsDiffFromIncome, round($campaign->income - $spendsSum,2));
        $this->assertEquals($responseContent->savingAchievements, round($wantedAmountsSum - $spendsSum ,2));
    }
}
