<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BankBeneficiary extends Model
{
    protected $fillable = ['name'];
    
    public function customBeneficiaries() { 
        return $this->belongsToMany('App\CustomBeneficiary');
    }
}
