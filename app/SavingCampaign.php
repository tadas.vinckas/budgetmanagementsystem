<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavingCampaign extends Model
{
    protected $fillable = ['dateFrom', 'dateTo', 'income', 'active'];

    public function componentCategories()
    {
        return $this->belongsToMany(ComponentCategory::class)->withPivot('wanted_amount');
    }

    public function scopeOwnUser($query)
    {
        return $query->where('user_id', auth('api')->user()->id);
    }

}
