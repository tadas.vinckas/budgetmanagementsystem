<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use File;

class CorrectFileType implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $extension = File::extension($value->getClientOriginalName());

        if ($extension == "csv") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Incorrect file type. File must be csv format';
    }
}
